﻿ {
     "Material": [
      {
          "ID": "71",
          "Name": "Technora",
          "Type": "T200",
          "LinearDensity": "1100",
          "ActualLinearDensity": "1120",
          "Selected":"#FFFFFF"
      },
      {
          "ID": "72",
          "Name": "Technora",
          "Type": "T200",
          "LinearDensity": "1670",
          "ActualLinearDensity": "1680",
          "Selected":"#FFFFFF"
      },
      {
          "ID": "73",
          "Name": "Technora",
          "Type": "T200",
          "LinearDensity": "3340",
          "ActualLinearDensity": "3370",
          "Selected":"#FFFFFF"
      },
      {
          "ID": "74",
          "Name": "Technora",
          "Type": "T200WL",
          "LinearDensity": "1670",
          "ActualLinearDensity": "1720",
          "Selected":"#FFFFFF"
      },
      {
          "ID": "75",
          "Name": "Technora",
          "Type": "T200W",
          "LinearDensity": "1670",
          "ActualLinearDensity": "1870",
          "Selected":"#FFFFFF"
      },
      {
          "ID": "76",
          "Name": "Technora",
          "Type": "T200WD",
          "LinearDensity": "1670",
          "ActualLinearDensity": "1870",
          "Selected":"#FFFFFF"
      },
      {
          "ID": "77",
          "Name": "Technora",
          "Type": "T221",
          "LinearDensity": "1670",
          "ActualLinearDensity": "1690",
          "Selected":"#FFFFFF"
      },
      {
          "ID": "78",
          "Name": "Technora",
          "Type": "T240",
          "LinearDensity": "440",
          "ActualLinearDensity": "440",
          "Selected":"#FFFFFF"
      },
      {
          "ID": "79",
          "Name": "Technora",
          "Type": "T240",
          "LinearDensity": "1100",
          "ActualLinearDensity": "1100",
          "Selected":"#FFFFFF"
      },
      {
          "ID": "80",
          "Name": "Technora",
          "Type": "T240",
          "LinearDensity": "1670",
          "ActualLinearDensity": "1680",
          "Selected":"#FFFFFF"
      },
      {
          "ID": "81",
          "Name": "Technora",
          "Type": "T240",
          "LinearDensity": "3340",
          "ActualLinearDensity": "3340",
          "Selected":"#FFFFFF"
      },
      {
          "ID": "82",
          "Name": "Technora",
          "Type": "T240",
          "LinearDensity": "6680",
          "ActualLinearDensity": "6680",
          "Selected":"#FFFFFF"
      },
      {
          "ID": "83",
          "Name": "Technora",
          "Type": "T240B",
          "LinearDensity": "440",
          "ActualLinearDensity": "440",
          "Selected":"#FFFFFF"
      },
      {
          "ID": "84",
          "Name": "Technora",
          "Type": "T240B",
          "LinearDensity": "830",
          "ActualLinearDensity": "830",
          "Selected":"#FFFFFF"
      },
      {
          "ID": "85",
          "Name": "Technora",
          "Type": "T240B",
          "LinearDensity": "1100",
          "ActualLinearDensity": "1100",
          "Selected":"#FFFFFF"
      },
      {
          "ID": "86",
          "Name": "Technora",
          "Type": "T240B",
          "LinearDensity": "1670",
          "ActualLinearDensity": "1680",
          "Selected":"#FFFFFF"
      }
    ]
}

