{
    "Material": [
        {
            "ID": "1",
            "Name": "Twaron",
            "Type": "1000",
            "LinearDensity": "840",
            "ActualLinearDensity": "860",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "2",
            "Name": "Twaron",
            "Type": "1000",
            "LinearDensity": "1100",
            "ActualLinearDensity": "1135",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "3",
            "Name": "Twaron",
            "Type": "1000",
            "LinearDensity": "1680",
            "ActualLinearDensity": "1735",
            "Selected":"#FFFFFF"
        
        },
        {
            "ID": "4",
            "Name": "Twaron",
            "Type": "1000",
            "LinearDensity": "3360",
            "ActualLinearDensity": "3460",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "5",
            "Name": "Twaron",
            "Type": "1005",
            "LinearDensity": "1680",
            "ActualLinearDensity": "1710",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "6",
            "Name": "Twaron",
            "Type": "1005",
            "LinearDensity": "3360",
            "ActualLinearDensity": "3560",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "7",
            "Name": "Twaron",
            "Type": "1005",
            "LinearDensity": "5040",
            "ActualLinearDensity": "5300",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "8",
            "Name": "Twaron",
            "Type": "1006",
            "LinearDensity": "1680",
            "ActualLinearDensity": "1760",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "9",
            "Name": "Twaron",
            "Type": "1006",
            "LinearDensity": "3360",
            "ActualLinearDensity": "3620",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "10",
            "Name": "Twaron",
            "Type": "1006",
            "LinearDensity": "5040",
            "ActualLinearDensity": "5570",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "11",
            "Name": "Twaron",
            "Type": "1008",
            "LinearDensity": "1100",
            "ActualLinearDensity": "1135",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "12",
            "Name": "Twaron",
            "Type": "1008",
            "LinearDensity": "1680",
            "ActualLinearDensity": "1730",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "13",
            "Name": "Twaron",
            "Type": "1008",
            "LinearDensity": "2520",
            "ActualLinearDensity": "2600",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "14",
            "Name": "Twaron",
            "Type": "1014",
            "LinearDensity": "1100",
            "ActualLinearDensity": "1135",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "15",
            "Name": "Twaron",
            "Type": "1014",
            "LinearDensity": "1680",
            "ActualLinearDensity": "1730",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "16",
            "Name": "Twaron",
            "Type": "1019",
            "LinearDensity": "1100",
            "ActualLinearDensity": "1135",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "17",
            "Name": "Twaron",
            "Type": "1025",
            "LinearDensity": "1100",
            "ActualLinearDensity": "1154",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "18",
            "Name": "Twaron",
            "Type": "2100",
            "LinearDensity": "1100",
            "ActualLinearDensity": "1135",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "19",
            "Name": "Twaron",
            "Type": "2100",
            "LinearDensity": "1680",
            "ActualLinearDensity": "1730",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "20",
            "Name": "Twaron",
            "Type": "2100",
            "LinearDensity": "2520",
            "ActualLinearDensity": "2600",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "21",
            "Name": "Twaron",
            "Type": "2200",
            "LinearDensity": "1210",
            "ActualLinearDensity": "1300",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "22",
            "Name": "Twaron",
            "Type": "2200",
            "LinearDensity": "1610",
            "ActualLinearDensity": "1710",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "23",
            "Name": "Twaron",
            "Type": "2200",
            "LinearDensity": "2420",
            "ActualLinearDensity": "2600",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "24",
            "Name": "Twaron",
            "Type": "2200",
            "LinearDensity": "3220",
            "ActualLinearDensity": "3420",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "25",
            "Name": "Twaron",
            "Type": "2200",
            "LinearDensity": "4830",
            "ActualLinearDensity": "5175",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "26",
            "Name": "Twaron",
            "Type": "2200",
            "LinearDensity": "6440",
            "ActualLinearDensity": "6860",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "27",
            "Name": "Twaron",
            "Type": "2200",
            "LinearDensity": "8050",
            "ActualLinearDensity": "8550",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "28",
            "Name": "Twaron",
            "Type": "2200",
            "LinearDensity": "9660",
            "ActualLinearDensity": "10260",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "29",
            "Name": "Twaron",
            "Type": "2200",
            "LinearDensity": "16100",
            "ActualLinearDensity": "17100",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "30",
            "Name": "Twaron",
            "Type": "D2200",
            "LinearDensity": "1210",
            "ActualLinearDensity": "1288",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "31",
            "Name": "Twaron",
            "Type": "D2200",
            "LinearDensity": "1610",
            "ActualLinearDensity": "1700",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "32",
            "Name": "Twaron",
            "Type": "D2200",
            "LinearDensity": "2420",
            "ActualLinearDensity": "2581",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "33",
            "Name": "Twaron",
            "Type": "D2200",
            "LinearDensity": "2680",
            "ActualLinearDensity": "2832",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "34",
            "Name": "Twaron",
            "Type": "D2200",
            "LinearDensity": "3220",
            "ActualLinearDensity": "3395",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "35",
            "Name": "Twaron",
            "Type": "D2200",
            "LinearDensity": "4830",
            "ActualLinearDensity": "5100",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "36",
            "Name": "Twaron",
            "Type": "D2200",
            "LinearDensity": "6440",
            "ActualLinearDensity": "6790",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "37",
            "Name": "Twaron",
            "Type": "D2200",
            "LinearDensity": "8050",
            "ActualLinearDensity": "8553",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "38",
            "Name": "Twaron",
            "Type": "D2200",
            "LinearDensity": "9660",
            "ActualLinearDensity": "10185",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "39",
            "Name": "Twaron",
            "Type": "D2200",
            "LinearDensity": "16100",
            "ActualLinearDensity": "17074",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "40",
            "Name": "Twaron",
            "Type": "D2211",
            "LinearDensity": "930",
            "ActualLinearDensity": "960",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "41",
            "Name": "Twaron",
            "Type": "2226",
            "LinearDensity": "1210",
            "ActualLinearDensity": "1300",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "42",
            "Name": "Twaron",
            "Type": "2226",
            "LinearDensity": "1610",
            "ActualLinearDensity": "1710",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "43",
            "Name": "Twaron",
            "Type": "D2226",
            "LinearDensity": "2420",
            "ActualLinearDensity": "2600",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "44",
            "Name": "Twaron",
            "Type": "D2226",
            "LinearDensity": "3220",
            "ActualLinearDensity": "3420",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "45",
            "Name": "Twaron",
            "Type": "2300",
            "LinearDensity": "1100",
            "ActualLinearDensity": "1135",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "46",
            "Name": "Twaron",
            "Type": "2300",
            "LinearDensity": "1680",
            "ActualLinearDensity": "1730",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "47",
            "Name": "Twaron",
            "Type": "2300",
            "LinearDensity": "2520",
            "ActualLinearDensity": "2600",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "48",
            "Name": "Twaron",
            "Type": "2300",
            "LinearDensity": "3360",
            "ActualLinearDensity": "3500",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "49",
            "Name": "Twaron",
            "Type": "2300",
            "LinearDensity": "16800",
            "ActualLinearDensity": "17500",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "50",
            "Name": "Twaron",
            "Type": "D3052",
            "LinearDensity": "1610",
            "ActualLinearDensity": "1765",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "51",
            "Name": "Twaron",
            "Type": "D3052",
            "LinearDensity": "2420",
            "ActualLinearDensity": "2700",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "52",
            "Name": "Twaron",
            "Type": "D3052",
            "LinearDensity": "2680",
            "ActualLinearDensity": "2950",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "53",
            "Name": "Twaron",
            "Type": "D3052",
            "LinearDensity": "3220",
            "ActualLinearDensity": "3540",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "54",
            "Name": "Twaron",
            "Type": "D3052",
            "LinearDensity": "4830",
            "ActualLinearDensity": "5410",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "55",
            "Name": "Twaron",
            "Type": "D3052",
            "LinearDensity": "8050",
            "ActualLinearDensity": "8865",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "56",
            "Name": "Twaron",
            "Type": "D3052",
            "LinearDensity": "9660",
            "ActualLinearDensity": "10615",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "57",
            "Name": "Twaron",
            "Type": "D3052",
            "LinearDensity": "12880",
            "ActualLinearDensity": "14200",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "58",
            "Name": "Twaron",
            "Type": "D3053",
            "LinearDensity": "9660",
            "ActualLinearDensity": "9765",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "59",
            "Name": "Twaron",
            "Type": "D3055",
            "LinearDensity": "1610",
            "ActualLinearDensity": "1630",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "60",
            "Name": "Twaron",
            "Type": "D3056",
            "LinearDensity": "930",
            "ActualLinearDensity": "1080",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "61",
            "Name": "Twaron",
            "Type": "D3057",
            "LinearDensity": "420",
            "ActualLinearDensity": "480",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "62",
            "Name": "Twaron",
            "Type": "D3057",
            "LinearDensity": "550",
            "ActualLinearDensity": "600",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "63",
            "Name": "Twaron",
            "Type": "D3057",
            "LinearDensity": "1100",
            "ActualLinearDensity": "1200",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "64",
            "Name": "Twaron",
            "Type": "D3200",
            "LinearDensity": "2640",
            "ActualLinearDensity": "2739",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "65",
            "Name": "Twaron",
            "Type": "D3200",
            "LinearDensity": "5280",
            "ActualLinearDensity": "5478",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "66",
            "Name": "Twaron",
            "Type": "D3200",
            "LinearDensity": "7920",
            "ActualLinearDensity": "8217",
            "Selected":"#FFFFFF"
        },
        {
            "ID": "67",
            "Name": "Twaron",
            "Type": "D3200",
            "LinearDensity": "13200",
            "ActualLinearDensity": "13747",
            "Selected":"#FFFFFF"
        }
    ]
}