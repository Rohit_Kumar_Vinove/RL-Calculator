﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace RL_Calculator
{
    [TemplatePart(Name = ElementLeftSideMenu, Type = typeof(FrameworkElement))]
    [TemplatePart(Name = ElementRightSideMenu, Type = typeof(FrameworkElement))]
    [TemplatePart(Name = ElementContentSelector, Type = typeof(Selector))]
    [TemplatePart(Name = ElementDisableContentOverlay, Type = typeof(Border))]
    public sealed class SlideInMenuContentControl : ContentControl
    {
        public static readonly DependencyProperty LeftMenuContentProperty =
            DependencyProperty.Register("LeftMenuContent", typeof(object), typeof(SlideInMenuContentControl), new PropertyMetadata(null));

        public static readonly DependencyProperty RightMenuContentProperty =
            DependencyProperty.Register("RightMenuContent", typeof(object), typeof(SlideInMenuContentControl), new PropertyMetadata(null));

        public static readonly DependencyProperty MenuStateProperty =
            DependencyProperty.Register("MenuState", typeof(MenuState), typeof(SlideInMenuContentControl), new PropertyMetadata(MenuState.Both, OnMenuStateChanged));

        public static readonly DependencyProperty RightSideMenuWidthProperty =
            DependencyProperty.Register("RightSideMenuWidth", typeof(double), typeof(SlideInMenuContentControl), new PropertyMetadata(0.0));

        public static readonly DependencyProperty LeftSideMenuWidthProperty =
            DependencyProperty.Register("LeftSideMenuWidth", typeof(double), typeof(SlideInMenuContentControl), new PropertyMetadata(250.0));

        private const string ElementLeftSideMenu = "ContentLeftSideMenu";
        private const string ElementRightSideMenu = "ContentRightSideMenu";
        private const string ElementContentSelector = "ContentSelector";
        private const string ElementDisableContentOverlay = "DisableContentOverlay";

        private FrameworkElement leftSideMenu;
        private FrameworkElement rightSideMenu;
        private Selector contentSelector;
        private Border disableContentOverlay;
        private bool updateFlipView;

        public SlideInMenuContentControl()
        {
            this.DefaultStyleKey = typeof(SlideInMenuContentControl);
        }

        public double LeftSideMenuWidth
        {
            get { return (double)GetValue(LeftSideMenuWidthProperty); }
            set { SetValue(LeftSideMenuWidthProperty, value); }
        }

        public double RightSideMenuWidth
        {
            get { return (double)GetValue(RightSideMenuWidthProperty); }
            set { SetValue(RightSideMenuWidthProperty, value); }
        }

        public MenuState MenuState
        {
            get { return (MenuState)GetValue(MenuStateProperty); }
            set { SetValue(MenuStateProperty, value); }
        }

        public object LeftMenuContent
        {
            get { return (object)GetValue(LeftMenuContentProperty); }
            set { SetValue(LeftMenuContentProperty, value); }
        }

        public object RightMenuContent
        {
            get { return (object)GetValue(RightMenuContentProperty); }
            set { SetValue(RightMenuContentProperty, value); }
        }

        public void GoToMenuState(ActiveState state)
        {
            updateFlipView = true;

            switch (state)
            {
                case ActiveState.Left:
                    if (MenuState != MenuState.Right)
                    {
                        contentSelector.SelectedIndex = 0;
                        MainPage.isMenuOpen = false;
                    }
                    break;
                case ActiveState.Right:
                    if (MenuState == MenuState.Right)
                    {
                        contentSelector.SelectedIndex = 1;
                        MainPage.isMenuOpen = false;
                    }

                    else if (MenuState == MenuState.Both)
                    {
                        contentSelector.SelectedIndex = 2;
                        //MainPage.isMenuOpen = true;
                    }
                    break;
                default:
                    break;
            }
        }

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            contentSelector = GetTemplateChild(ElementContentSelector) as Selector;
            leftSideMenu = GetTemplateChild(ElementLeftSideMenu) as FrameworkElement;
            rightSideMenu = GetTemplateChild(ElementRightSideMenu) as FrameworkElement;
            disableContentOverlay = GetTemplateChild(ElementDisableContentOverlay) as Border;
            contentSelector.SelectionChanged += ContentSelector_SelectionChanged;
            SetMenuVisibility();
        }

        private static void OnMenuStateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as SlideInMenuContentControl;
            control.SetMenuVisibility();
        }

        private void SetMenuVisibility()
        {
            //rightSideMenu != null && mene remove kra h Rohit
            if (rightSideMenu != null && leftSideMenu != null && contentSelector != null)
            {
                switch (MenuState)
                {
                    case MenuState.Left:
                        rightSideMenu.Visibility = Visibility.Collapsed;
                        leftSideMenu.Visibility = Visibility.Visible;
                        contentSelector.SelectedIndex = 1;
                        //  MainPage.isMenuOpen = true;
                        break;
                    case MenuState.Right:
                        rightSideMenu.Visibility = Visibility.Visible;
                        leftSideMenu.Visibility = Visibility.Collapsed;
                        contentSelector.SelectedIndex = 0;
                        //  MainPage.isMenuOpen =false;
                        break;
                    case MenuState.Both:
                        //rightSideMenu.Visibility = Visibility.Visible;
                        leftSideMenu.Visibility = Visibility.Visible;
                        contentSelector.SelectedIndex = 2;
                        break;
                    default:
                        break;
                }
            }
        }

        private void ContentSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            try
            {
                var x = MainPage.isMenuOpen;
                var y = Units.isMenuOpen;
                var z = Info.isMenuOpen;
                if (x == true)
                {
                    MainPage.isMenuOpen = false;

                }
                //else
                //{
                //    MainPage.isMenuOpen = true;
                //}
                if (y == true)
                {
                    Units.isMenuOpen = false;
                }
                //else
                //{
                //    Units.isMenuOpen = true;
                //}

                if (z == true)
                {
                    Info.isMenuOpen = false;
                }
                //else
                //{
                //    Info.isMenuOpen = true;
                //}

            }
            catch (Exception ex)
            { }

            switch (MenuState)
            {
                //        case MenuState.Left:
                //            try
                //            {
                //                MainPage.LeftmenuTapped.Visibility = Visibility.Collapsed;
                //                MainPage.Leftmenu1Tapped.Visibility = Visibility.Visible;
                //                Units.LeftmenuTapped.Visibility = Visibility.Visible;
                //                Units.Leftmenu1Tapped.Visibility = Visibility.Collapsed;
                //                Info.LeftmenuTapped.Visibility = Visibility.Visible;
                //                Info.Leftmenu1Tapped.Visibility = Visibility.Collapsed;

                //            }
                //            catch (Exception Ex) { }
                //            if (contentSelector.SelectedIndex == 0)
                //            {
                //                disableContentOverlay.Visibility = Visibility.Visible;
                //            }
                //            else
                //            {
                //                disableContentOverlay.Visibility = Visibility.Collapsed;
                //            }
                //            break;
                //        case MenuState.Right:
                //            try
                //            {
                //                MainPage.LeftmenuTapped.Visibility = Visibility.Visible;
                //                MainPage.Leftmenu1Tapped.Visibility = Visibility.Collapsed;
                //                Units.LeftmenuTapped.Visibility = Visibility.Collapsed;
                //                Units.Leftmenu1Tapped.Visibility = Visibility.Visible;
                //                Info.LeftmenuTapped.Visibility = Visibility.Collapsed;
                //                Info.Leftmenu1Tapped.Visibility = Visibility.Visible;

                //            }
                //            catch (Exception Ex) { }
                //            if (contentSelector.SelectedIndex == 0)
                //            {
                //                disableContentOverlay.Visibility = Visibility.Collapsed;
                //            }
                //            else
                //            {
                //                disableContentOverlay.Visibility = Visibility.Visible;
                //            }
                //            break;

                case MenuState.Both:
                    {
                       
                        try
                        {
                             if (contentSelector.SelectedIndex == 0)
                             {
                                 MainPage.isMenuOpen = true;
                                 Units.isMenuOpen = true;
                                 Info.isMenuOpen = true;
                             }
                        }


                        catch (Exception Ex)
                        { }



                        break;
                        //        default:
                        //            break;
                        //    }
                    }
            }
        }
    }
}
