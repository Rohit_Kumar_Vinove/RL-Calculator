﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;

using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;

namespace RL_Calculator.Common
{
    public class BoolToImageSource : IValueConverter
    {
        public Windows.UI.Xaml.Media.ImageBrush FalseValue;
        public Windows.UI.Xaml.Media.ImageBrush TrueValue;
        //public System.Windows.Media.ImageSource FalseValue;
        //public System.Windows.Media.ImageSource TrueValue;

        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            try
            {
                BitmapSource TrueValue = new BitmapImage(new Uri("/Assets/red-check-mark-md.png", UriKind.RelativeOrAbsolute));
                BitmapSource FalseValue = new BitmapImage(new Uri("/Assets/120px-BLANK_ICON.png", UriKind.RelativeOrAbsolute));

                if (value == null)
                    return FalseValue;
                else
                    return (bool)value ? TrueValue : FalseValue;
            }
            catch { return FalseValue; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            return value != null ? value.Equals(TrueValue) : false;
        }
    }
}
