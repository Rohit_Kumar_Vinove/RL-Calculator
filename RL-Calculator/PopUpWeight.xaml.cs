﻿using RL_Calculator.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace RL_Calculator
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PopUpWeight : Page
    {
        public PopUpWeight(string m)
        {
            this.InitializeComponent();
            var cult = CultureInfo.CurrentCulture.ToString();
            if (cult.Contains("zh"))
            {
                HeaderPopUpWight.Source = new BitmapImage(new Uri("ms-appx:///Assets/filterpopupweightunitchinese.png"));
            }
            else
            {
                HeaderPopUpWight.Source = new BitmapImage(new Uri("ms-appx:///Assets/filter-popup-weight-unit.png"));
            }

            Units.isMenuOpen = false;
            ScaleFactor();
            PopulateWeight(m);

        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }



        public void PopulateWeight(string m)
        {


            //<---------------long code unit------------>

            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            string gram = loader.GetString("lblUnitGramWithCode");
            var meter = loader.GetString("lblUnitMeterWithCode");
            string feet = loader.GetString("lblUnitFeetWithCode");
            string yards = loader.GetString("lblUnitYardsWithCode");
            string kilograms = loader.GetString("lblUnitKilogramWithCode");
            string kilometers = loader.GetString("lblUnitKilometersWithCode");
            string pounds = loader.GetString("lblUnitPoundWithCode");
            string miles = loader.GetString("lblUnitMilesWithCode");

            //<----EndOfStreamException------>
            var gr = loader.GetString("lblShortGram");
            var me = loader.GetString("lblShortMeter");
            var ft = loader.GetString("lblShortFeet");
            var yd = loader.GetString("lblShortYards");
            var kg = loader.GetString("lblShortKilogram");
            var km = loader.GetString("lblShortKilometers");
            var denier = loader.GetString("lblDenier");
            var dtex = loader.GetString("lblDtex");

            List<Weight> Weight = new List<Weight>();
            if (m == "gram")
            {
                if (oldColorWeight.name == gram)
                {
                    Weight.Add(new Weight { ID = 1, value = gram, Color = "red" });
                }

                else
                {
                    if (oldColorWeight.name != kilograms)
                    {
                        Weight.Add(new Weight { ID = 1, value = gram, Color = "red" });
                    }
                    else
                    Weight.Add(new Weight { ID = 1, value = gram , Color = "white" });
                }
                 
                if (oldColorWeight.name == kilograms)
                {
                    Weight.Add(new Weight { ID = 2, value = kilograms, Color = "red" });
                }

                else
                {
                    Weight.Add(new Weight { ID = 2, value = kilograms, Color = "white" });
                }
               
            }
            else
            {
                
                if (oldColorWeight.name == gram)
                {
                    Weight.Add(new Weight { ID = 1, value = gram, Color = "red" });
                }
                    
                else
                {
                    Weight.Add(new Weight { ID = 1, value = gram, Color = "white" });
                }
                   
                if (oldColorWeight.name == kilograms)
                {
                    Weight.Add(new Weight { ID = 2, value = kilograms, Color = "red" });
                }
                   
                else
                {
                    Weight.Add(new Weight { ID = 2, value = kilograms, Color = "white" });
                }
                   
                if (oldColorWeight.name == pounds)
                {
                    Weight.Add(new Weight { ID = 3, value = pounds, Color = "red" });
                }

                else
                {
                    if (oldColorWeight.name != kilograms && oldColorWeight.name != gram)
                    {
                        Weight.Add(new Weight { ID = 3, value = pounds, Color = "red" });
                    }
                    else
                    Weight.Add(new Weight { ID = 3, value = pounds, Color = "white" });
                }
                  

               
            
            }
            lstweight.ItemsSource = Weight;
           
        }

        /// <summary>
        /// function to closethe popup box
        /// </summary>
        private void ClosePopup()
        {
            Popup popupHelloWorld = this.Parent as Popup;
            popupHelloWorld.IsOpen = false;
            Units.isMenuOpen = false;
        }

        /// <summary>
        /// Event to close popup
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">RoutedEventArgs</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {

            ClosePopup();
        }

        private void chkbxWeight_Checked(object sender, RoutedEventArgs e)
        {


            //<---------------long code unit------------>

            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            string gram = loader.GetString("lblUnitGramWithCode");
            var meter = loader.GetString("lblUnitMeterWithCode");
            string feet = loader.GetString("lblUnitFeetWithCode");
            string yards = loader.GetString("lblUnitYardsWithCode");
            string kilograms = loader.GetString("lblUnitKilogramWithCode");
            string kilometers = loader.GetString("lblUnitKilometersWithCode");
            string pounds = loader.GetString("lblUnitPoundWithCode");
            string miles = loader.GetString("lblUnitMilesWithCode");

            //<----EndOfStreamException------>
            var gr = loader.GetString("lblShortGram");
            var me = loader.GetString("lblShortMeter");
            var ft = loader.GetString("lblShortFeet");
            var yd = loader.GetString("lblShortYards");
            var kg = loader.GetString("lblShortKilogram");
            var km = loader.GetString("lblShortKilometers");
            var lbs = loader.GetString("lblShortPound");
            var denier = loader.GetString("lblDenier");
            var dtex = loader.GetString("lblDtex");



            RadioButton chk2 = (RadioButton)sender;
            OldValue.boobinTube = MainPage.boobinTube.Text;
            OldValue.runningTube = MainPage.runningTube.Text;
            OldValue.runningbobbinweight = MainPage.runningbobbinweight.Text;
            string value = chk2.Tag.ToString();
            switch (value)
            {
                case "gram (gr)":
                    UnitConverter.boobinTube = "gr";

                    UnitConverter.runningTube = "gr";
                    UnitConverter.runningbobbinweight = "gr";


                 
                    break;
                case "克":
                    UnitConverter.boobinTube = "克";

                    UnitConverter.runningTube = "克";
                    UnitConverter.runningbobbinweight = "克";



                    break;

                case "kilograms (kg)":
                    UnitConverter.boobinTube = "kg";

                    UnitConverter.runningTube = "kg";
                    UnitConverter.runningbobbinweight = "kg";
                    
                 
                    break;
                case "千克":
                    UnitConverter.boobinTube = "千克";

                    UnitConverter.runningTube = "千克";
                    UnitConverter.runningbobbinweight = "千克";


                    break;
                case "pound (lbs)":
                    UnitConverter.boobinTube = "lbs";

                    UnitConverter.runningTube = "lbs";
                    UnitConverter.runningbobbinweight = "lbs";
                    break;

                case "磅":
                    UnitConverter.boobinTube = "磅";

                    UnitConverter.runningTube = "磅";
                    UnitConverter.runningbobbinweight = "磅";
                    break;

            }

            if (chk2.Tag.ToString()=="gram")
            {

            }
            else if (chk2.Tag.ToString() == denier)
            { 
            UnitConverter.bobbinRLength = ft;
            UnitConverter.bobbinweightLinerd = denier;
            UnitConverter.boobinTube = lbs;

            UnitConverter.runningTube = lbs;
            UnitConverter.runninglengthLinerd = denier;
            UnitConverter.runningbobbinweight = lbs;
            }
           
            //   MainPage cs =new MainPage();
            //TextBox txt = (TextBox)cs.FindName("MaterialBox");
            //  txt.Text=chk2.Tag.ToString();
             Units.weight1.Text= chk2.Tag.ToString();
             oldColorWeight.name = chk2.Content.ToString();
             oldColorWeight.Color = true;
            chk2.IsChecked = true;
            ClosePopup();
        }

        public void ScaleFactor()
        {
            var scaleFactor = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;
            var WidthResolution = Window.Current.Bounds.Width * scaleFactor;
            var Heightresolution = Window.Current.Bounds.Height * scaleFactor;
            if (scaleFactor == 2.2)
            {
                //Richtxtblk.FontSize = 19;
                //Richtxtblk1.FontSize = 19;
                //Richtxtblk2.FontSize = 18;
                weightcontainer.Width = 500;
                weightcontainer.Height = 900;
            }
            else if (scaleFactor == 2.4)
            {
                //Infocontainer.Margin = new Thickness(-55, 0, 0, 0);
                //Richtxtblk.FontSize = 18;
                //Richtxtblk1.FontSize = 18;
                //Richtxtblk2.FontSize = 17;
                weightcontainer.Width = 460;
                weightcontainer.Height = 800;
            }
            else if (scaleFactor == 1.8)
            {
                weightcontainer.Width = 400;
                weightcontainer.Height = 700;
            }
            else if (scaleFactor == 2.0)
            {
                weightcontainer.Width = 390;
                weightcontainer.Height = 640;
            }
            else if (scaleFactor == 1.2 && WidthResolution != 540.0 && Heightresolution != 960.0)
            {
                weightcontainer.Width = 400;
                weightcontainer.Height = 680;
            }
            else if (scaleFactor == 1.2 && WidthResolution == 540.0 && Heightresolution == 960.0)
            {
                weightcontainer.Width = 450;
                weightcontainer.Height = 770;

            }     
           
            else if (scaleFactor == 1.6)
            {
                weightcontainer.Width = 450;
                weightcontainer.Height = 799.6;
            }
            else if (scaleFactor == 3.4)
            {
                weightcontainer.Width = 430;
                weightcontainer.Height = 760;
            }
            else
            {
                getScreenInfo();
            }
        }



        public void getScreenInfo()
        {
            var height = Window.Current.Bounds.Height * (0.73 - 0.008);
            var width = Window.Current.Bounds.Width*0.8;
            //var Actualheigt = height - 540;

            //var lgt = height - Actualheigt;
            weightcontainer.Width = width;
            weightcontainer.Height = height;
        }
    }

}
