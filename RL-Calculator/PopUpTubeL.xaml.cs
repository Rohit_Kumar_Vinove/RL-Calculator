﻿using RL_Calculator.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace RL_Calculator
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PopUpTubeL : Page
    {
        public PopUpTubeL()
        {
            this.InitializeComponent();
            var cult = CultureInfo.CurrentCulture.ToString();
            if (cult.Contains("zh"))
            {
                HeadingPopupTube.Source = new BitmapImage(new Uri("ms-appx:///Assets/filterpopuptubeschinese.png"));
            }
            else
            {
                HeadingPopupTube.Source = new BitmapImage(new Uri("ms-appx:///Assets/filter-popup-tubes.png"));
            }

            ScaleFactor();
            PopulateData();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }


        public async Task PopulateData()
        {


            string content = String.Empty;

            //var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync("data.txt");

            var folder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            var file = await folder.GetFileAsync("Tube.js");
            var contents = await Windows.Storage.FileIO.ReadTextAsync(file);

            //var path = Path.GetPathRoot("/data.js");

            var jData = JsonObject.Parse(contents);
            var jArray = jData.GetNamedArray("Tube");
            List<Tube> items = new List<Tube>();
            foreach (var item in jArray)
            {
                var itemObject = item.GetObject();
                Tube mate = new Tube();


                string itemDimension = itemObject.GetNamedString("Dimensions");
                string itemWeight = itemObject.GetNamedString("Weight");
                string Selected = itemObject.GetNamedString("Selected");


                mate.Dimensions = itemDimension;
                if (mate.Dimensions == oldColorTubeL.name)
                    mate.Selected = "Red";
                else
                    mate.Selected = Selected;
                mate.Weight = itemWeight;

                items.Add(mate);

            }
            //List<Material> items = JsonConvert.DeserializeObject<List<Material>>(content);
            lst_books.ItemsSource = items;

        }
        private void chkbx_Checked(object sender, RoutedEventArgs e)
        {
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();

            //<--------------shortcodeunit-------------->
            var gr = loader.GetString("lblShortGram");
            var m = loader.GetString("lblShortMeter");
            var ft = loader.GetString("lblShortFeet");
            var yd = loader.GetString("lblShortYards");
            var kg = loader.GetString("lblShortKilogram");
            var km = loader.GetString("lblShortKilometers");
            var lbs = loader.GetString("lblShortPound");
            var LxIDxOD = loader.GetString("lblHeaderTubeDimensions");

            RadioButton chk2 = (RadioButton)sender;
            if (Converters.UnitConverter.runningTube == kg)
            {

                MainPage.tubeL.Text = (Convert.ToDouble(chk2.Tag.ToString()) / 1000).ToString(".00");
            }
            else if (Converters.UnitConverter.runningTube == lbs)

                MainPage.tubeL.Text = (((Convert.ToDouble(chk2.Tag.ToString()) / 453.59237) * 100000) / 100000).ToString(".00");

            else
            MainPage.tubeL.Text = chk2.Tag.ToString();

            MainPage.runningtubitem.Text = chk2.Content.ToString()+" "+LxIDxOD;
            oldColorTubeL.name = chk2.Content.ToString();
            oldColorTubeL.Color = true;
           
            ClosePopup();
        }



        private void ClosePopup()
        {
            Popup popupHelloWorld = this.Parent as Popup;
            popupHelloWorld.IsOpen = false;
            MainPage.isMenuOpen = false;
        }

        /// <summary>
        /// Event to close popup
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">RoutedEventArgs</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            ClosePopup();
        }


        public void ScaleFactor()
        {
            var scaleFactor = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;
            var WidthResolution = Window.Current.Bounds.Width * scaleFactor;
            var Heightresolution = Window.Current.Bounds.Height * scaleFactor;
            if (scaleFactor == 2.2)
            {

                TubePopupView.Width = 420;
                TubePopupView.Height = 760;
            }
            else if (scaleFactor == 2.4)
            {

                TubePopupView.Width = 380;
                TubePopupView.Height = 690;
            }
            else if (scaleFactor == 1.8)
            {
                TubePopupView.Width = 320;
                TubePopupView.Height = 600;
            }
            else if (scaleFactor == 2.0)
            {
                TubePopupView.Width = 315;
                TubePopupView.Height = 535;
            }
            else if (scaleFactor == 1.2 && WidthResolution != 540.0 && Heightresolution != 960.0)
            {
                TubePopupView.Width = 330;
                TubePopupView.Height = 560;
            }
            else if (scaleFactor == 1.2 && WidthResolution == 540.0 && Heightresolution == 960.0)
            {
                TubePopupView.Width = 380;
                TubePopupView.Height = 650;

            }   

            //else if (scaleFactor == 1.2)
            //{
            //    TubePopupView.Width = 330;
            //    TubePopupView.Height = 560;
            //}
            else if (scaleFactor == 1.6)
            {
                TubePopupView.Width = 370;
                TubePopupView.Height = 688;
            }
            else if (scaleFactor == 3.4)
            {
                TubePopupView.Width = 350;
                TubePopupView.Height = 660;
            }
            else
            {
                getScreenInfo();
            }
        }

        public void getScreenInfo()
        {
            var height = Window.Current.Bounds.Height * (0.73 - 0.008);
            var width = Window.Current.Bounds.Width*0.8;
            //var Actualheigt = height - 540;

            //var lgt = height - Actualheigt;
            TubePopupView.Width = width;
            TubePopupView.Height = height;
        }
    }
}
