﻿using RL_Calculator.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace RL_Calculator
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PopUpLength : Page
    {
        public PopUpLength(string m)
        {            
            this.InitializeComponent();
            var cult = CultureInfo.CurrentCulture.ToString();
            if (cult.Contains("zh"))
            {
                HeaderPopUpLenght.Source = new BitmapImage(new Uri("ms-appx:///Assets/filterpopuplengthunitchinese.png"));
            }
            else
            {
                HeaderPopUpLenght.Source = new BitmapImage(new Uri("ms-appx:///Assets/filter-popup-length-unit.png"));
            }   

            Units.isMenuOpen = false;
            ScaleFactor();
            PopulateWeight(m);
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        //public static List<Length> Length = new List<Length>();

        public void PopulateWeight(string m)
        {
            //<---------------long code unit------------>
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            var gram = loader.GetString("lblUnitGramWithCode");
            var meter = loader.GetString("lblUnitMeterWithCode");
            var feet = loader.GetString("lblUnitFeetWithCode");
            var yards = loader.GetString("lblUnitYardsWithCode");
            var kilogram = loader.GetString("lblUnitKilogramWithCode");
            var kilometers = loader.GetString("lblUnitKilometersWithCode");
            var pounds = loader.GetString("lblUnitPoundWithCode");
            var miles = loader.GetString("lblUnitMilesWithCode");

            List<Length> Length = new List<Length>();
            if (m == "meter")
            {
                

                if (oldColorLength.name == meter)
                {
                    Length.Add(new Length { ID = 1, value1 = meter, Color1 = "red" });
                }

                else
                {
                    if (oldColorLength.name != kilometers)
                    {
                        Length.Add(new Length { ID = 1, value1 = meter, Color1 = "red" });
                    }
                    else
                    Length.Add(new Length { ID = 1, value1 = meter, Color1 = "white" });
                }
                 

                if (oldColorLength.name == kilometers){
                    Length.Add(new Length { ID = 2, value1 = kilometers, Color1 = "red" });
                }

               
                 else
                {

                    //if (oldColorLength.name != "")
                    //{
                    //    Length.Add(new Length { ID = 1, value1 = "meter (m)", Color1 = "red" });
                    //}

                    //else 
                    Length.Add(new Length { ID = 2, value1 = kilometers, Color1 = "white" });
                }

                     
            }
            else
            {
                //if (Units.SValue == "")
                if (oldColorLength.name == feet){
                    Length.Add(new Length { ID = 1, value1 = feet, Color1 = "red" });
                }
                
                else
                {
                    if (oldColorLength.name != kilometers && oldColorLength.name != miles && oldColorLength.name != yards)
                    {
                        Length.Add(new Length { ID = 1, value1 = feet, Color1 = "red" });
                    }
                    else
                    Length.Add(new Length { ID = 1, value1 = feet, Color1 = "white" });
                }
                   
                if (oldColorLength.name == kilometers)
                {
                    Length.Add(new Length { ID = 2, value1 = kilometers, Color1 = "red" });
                }
              
                else
                {
                    Length.Add(new Length { ID = 2, value1 = kilometers, Color1 = "white" });
                }
                   
                if (oldColorLength.name == miles)
                {
                    Length.Add(new Length { ID = 3, value1 = miles, Color1 = "red" });
                }
              
                else
                {
                    Length.Add(new Length { ID = 3, value1 = miles, Color1 = "white" });
                }
                   
                if (oldColorLength.name == yards)
                {
                    Length.Add(new Length { ID = 4, value1 = yards, Color1 = "red" });
                }
             
                else
                {
                    Length.Add(new Length { ID = 4, value1 = yards, Color1 = "white" });
                
                }
               
               
            }

        
            lst_length.ItemsSource = Length;


        }

        /// <summary>
        /// function to closethe popup box
        /// </summary>
        private void ClosePopup()
        {
            Popup popupHelloWorld = this.Parent as Popup;
            popupHelloWorld.IsOpen = false;
            Units.isMenuOpen = false;
        }

        /// <summary>
        /// Event to close popup
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">RoutedEventArgs</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            ClosePopup();
        }

        private void chkbxLength_Checked(object sender, RoutedEventArgs e)
        {

            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();

            //<--------------shortcodeunit-------------->
            //var gr = loader.GetString("lblShortGram");
            //var m = loader.GetString("lblShortMeter");
            //var ft = loader.GetString("lblShortFeet");
            //var yd = loader.GetString("lblShortYards");
            //var kg = loader.GetString("lblShortKilogram");
            //var km = loader.GetString("lblShortKilometers");
            //<---------------long code unit------------>

            //   var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            //string gram = loader.GetString("lblUnitGramWithCode");
            //var meter = loader.GetString("lblUnitMeterWithCode");
            //string feet = loader.GetString("lblUnitFeetWithCode");
            //string yards = loader.GetString("lblUnitYardsWithCode");
            //string kilogram = loader.GetString("lblUnitKilogramWithCode");
            //string kilometers = loader.GetString("lblUnitKilometersWithCode");
            //string pounds = loader.GetString("lblUnitPoundWithCode");
            //string miles = loader.GetString("lblUnitMilesWithCode");

            //<----EndOfStreamException------>
            RadioButton chk2 = (RadioButton)sender;
            OldValue.bobbinRLength = MainPage.bobbinRLength.Text;
            string value = chk2.Tag.ToString();
            switch (value)
            {
                case "meter (m)":
                    UnitConverter.bobbinRLength = "m";
                    //UnitConverter.boobinTube = "m";
                    break;

                case "米":
                    UnitConverter.bobbinRLength = "米";
                    //UnitConverter.runningTube = "m";
                    //UnitConverter.runningbobbinweight = "m";
                    break;

                case "kilometers (km)":
                    UnitConverter.bobbinRLength = "km";
                    //UnitConverter.boobinTube = "km";
                  
                    //UnitConverter.runningTube = "km";
                    //UnitConverter.runningbobbinweight = "km";
                    break;
                case "千米":
                    UnitConverter.bobbinRLength = "千米";
                    break;

                case "feet (ft)":
                    UnitConverter.bobbinRLength = "ft";
                    //UnitConverter.boobinTube = "ft";
               
                    //UnitConverter.runningTube = "ft";
                    //UnitConverter.runningbobbinweight = "ft";
                    break;
                case "英尺":
                    UnitConverter.bobbinRLength = "英尺";
                    break;
                case "miles (mi)":
                    UnitConverter.bobbinRLength = "mi";
                    //UnitConverter.boobinTube = "mi";
     
                    //UnitConverter.runningTube = "mi";
                    //UnitConverter.runningbobbinweight = "mi";
                    break;
                case "英里":
                    UnitConverter.bobbinRLength = "英里";
                    break;
                case "yards (yd)":
                    UnitConverter.bobbinRLength = "yd";
                    //UnitConverter.boobinTube = "yd";
             
                    //UnitConverter.runningTube = "yd";
                    //UnitConverter.runningbobbinweight = "yd";
                    break;
                case "码":
                    UnitConverter.bobbinRLength = "码";
                    break;
                    //   MainPage cs =new MainPage();
                    //TextBox txt = (TextBox)cs.FindName("MaterialBox");
                    //  txt.Text=chk2.Tag.ToString();
            }
                    Units.lenth1.Text = chk2.Tag.ToString();
                    oldColorLength.name = chk2.Content.ToString();
                    oldColorLength.Color = true;

                    chk2.IsChecked = true;
                    ClosePopup();
            }

        public void ScaleFactor()
        {
            var scaleFactor = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;
            var WidthResolution = Window.Current.Bounds.Width * scaleFactor;
            var Heightresolution = Window.Current.Bounds.Height * scaleFactor;
            if (scaleFactor == 2.2)
            {
                //Richtxtblk.FontSize = 19;
                //Richtxtblk1.FontSize = 19;
                //Richtxtblk2.FontSize = 18;
                LengthContainer.Width = 500;
                LengthContainer.Height = 900;
            }
            else if (scaleFactor == 2.4)
            {
                //Infocontainer.Margin = new Thickness(-55, 0, 0, 0);
                //Richtxtblk.FontSize = 18;
                //Richtxtblk1.FontSize = 18;
                //Richtxtblk2.FontSize = 17;
                LengthContainer.Width = 460;
                LengthContainer.Height = 800;
            }
            else if (scaleFactor == 1.8)
            {
                LengthContainer.Width = 400;
                LengthContainer.Height = 700;
            }
            else if (scaleFactor == 2.0)
            {
                LengthContainer.Width = 390;
                LengthContainer.Height = 640;
            }

            else if (scaleFactor == 1.2 && WidthResolution != 540.0 && Heightresolution != 960.0)
            {
                LengthContainer.Width = 400;
                LengthContainer.Height = 680;
            }
            else if (scaleFactor == 1.2 && WidthResolution == 540.0 && Heightresolution == 960.0)
            {
                LengthContainer.Width = 450;
                LengthContainer.Height = 770;

            }          
            else if (scaleFactor == 1.6)
            {
                LengthContainer.Width = 450;
                LengthContainer.Height = 779.6;
            }
            else if (scaleFactor == 3.4)
            {
                LengthContainer.Width = 430;
                LengthContainer.Height = 760;
            }
            else
            {
                getScreenInfo();
            }
        }

        public void getScreenInfo()
        {
            var height = Window.Current.Bounds.Height * (0.73 - 0.008);
            var width = Window.Current.Bounds.Width*0.8;

            LengthContainer.Width = width;
            LengthContainer.Height = height;

        }

    }

    }

