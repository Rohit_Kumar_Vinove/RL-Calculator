﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace RL_Calculator
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Info : Page
    {
        public static bool isMenuOpen;
        public static Image LeftmenuTapped, Leftmenu1Tapped;
        public Info()
        {
            this.InitializeComponent();
            isMenuOpen = true;
            LeftmenuTapped = (Image)FindName("LeftButtonTapped");
           // Leftmenu1Tapped = (Image)FindName("LeftButton1Tapped");
            ScaleFactor();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        private void LeftButtonTapped_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //LeftButtonTapped.Visibility = Visibility.Collapsed;
            //LeftButton1Tapped.Visibility = Visibility.Visible;
            //slidein.GoToMenuState(ActiveState.Left);
            //{

            //}
            if (isMenuOpen)
            {
                slidein.GoToMenuState(ActiveState.Right);
                {

                }
                isMenuOpen = false;
            }
            else
            {
                slidein.GoToMenuState(ActiveState.Left);
                {

                }
                isMenuOpen = true;
            }

        }

        private void HomeButtonClick(object sender, TappedRoutedEventArgs e)
        {
           // MainPage.LeftmenuTapped.Visibility = Visibility.Visible;
            //NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            //NavigateUri = “/SecondPage.xaml”
            //Frame.Navigate(typeof(MainPage));
            //slidein.GoToMenuState(ActiveState.Right);
            this.Frame.Navigate(typeof(MainPage));
           // slidein.GoToMenuState(ActiveState.Right);
           // { }
            //Window.Current.Activate();

        }

        private void unitsButtonClick(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Units));
        }

        private void InfoButtonClick(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Info));
        }

        //private void LeftButton1Tapped_Tapped(object sender, TappedRoutedEventArgs e)
        //{
        //    slidein.GoToMenuState(ActiveState.Right);

        //    LeftButtonTapped.Visibility = Visibility.Visible;
        //    LeftButton1Tapped.Visibility = Visibility.Collapsed;
        //}


        public void ScaleFactor()
        {
            var scaleFactor = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;
            if (scaleFactor == 2.2)
            {
                Richtxtblk.FontSize = 19;
                Richtxtblk1.FontSize = 19;
                Richtxtblk2.FontSize = 18;
                infoScale.Width = 490;
                infoScale.Height = 900;
            }
            else  if (scaleFactor == 2.4)
            {
                //Infocontainer.Margin = new Thickness(-55, 0, 0, 0);
                Richtxtblk.FontSize = 18;
                Richtxtblk1.FontSize = 18;
                Richtxtblk2.FontSize = 17;
                infoScale.Width = 450;
                infoScale.Height = 780;
            }
            else if (scaleFactor == 1.8)
            {
                infoScale.Width = 400;
                infoScale.Height = 680;
            }
            else if (scaleFactor == 2.0)
            {
                infoScale.Width = 390;
                infoScale.Height = 640;
            }
            else if (scaleFactor == 1.2)
            {
                infoScale.Width = 400;
                infoScale.Height = 640;
            }
            //else if (scaleFactor == 1.6)
            //{
            //    infoScale.Width = 480;
            //    infoScale.Height = 770;
            //}
            else if (scaleFactor == 3.4)
            {
                infoScale.Width = 430;
                infoScale.Height = 720;
            }
            else
            {
                getScreenInfo();
            }
        }

        public void getScreenInfo()
        {
            var height = Window.Current.Bounds.Height;
            var width = Window.Current.Bounds.Width;
            //var Actualheigt = height - 540;

            //var lgt = height - Actualheigt;

            infoScale.Height = height;
            infoScale.Width = width;









            //var width = Window.Current.Bounds.Width * (int)DisplayProperties.ResolutionScale / 100;
            //var height = Window.Current.Bounds.Height * (int)DisplayProperties.ResolutionScale / 100;

            //var dpiy = DisplayInformation.GetForCurrentView().RawDpiY;
            //var dpix = DisplayInformation.GetForCurrentView().RawDpiX;

            //var screenDiagonal = Math.Sqrt(Math.Pow(width / dpix, 2) +
            //            Math.Pow(height / dpiy, 2));

            //return screenDiagonal.ToString();
            //pivotcontent.MaxWidth = width;
            //pivotcontent.MaxHeight = height;

            //    double dpix = -1.01;
            //    double screensize = -1.01;
            //    double dpiy = -1.01;
            //    Size res;
            //    try
            //    {
            //dpix = (double)DeviceExtendedProperties.GetValue("RawDpiX");
            //dpiy = (double)DeviceExtendedProperties.GetValue("RawDpiY");
            //res = (Size)DeviceExtendedProperties.GetValue("PhysicalScreenResolution");
            //screensize = Math.Sqrt(Math.Pow(res.Width / dpix, 2) + Math.Pow(res.Height / dpiy, 2));
            //    }
            //    catch (Exception e)
            //    {
            //    }
        }
    }


}
