﻿using RL_Calculator.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace RL_Calculator
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PopUpMaterialL : Page
    {
        static List<Material> LastItems = new List<Material>();

        public PopUpMaterialL()
        {
            this.InitializeComponent();
            var cult = CultureInfo.CurrentCulture.ToString();
            if (cult.Contains("zh"))
            {
                ImgLastUsed.Source = new BitmapImage(new Uri("ms-appx:///Assets/lastUsedChinese.png"));
            }
            else
            {
                ImgLastUsed.Source = new BitmapImage(new Uri("ms-appx:///Assets/LastUsed.png"));
            }



            ScaleFactor();
            PopulateData("Twaron");
            if (oldColorMaterialL.index == 1)
            {
                HomePagePivot.SelectedIndex = 1;
                PopulateData("Technora");

            }
            else if (oldColorMaterialL.index == 2)
            {

                HomePagePivot.SelectedIndex = 2;
                PopulateData("LastUsed");

            }
            else
            {

                HomePagePivot.SelectedIndex = 0;
              

            }

            
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void TabButtonClick(object sender, TappedRoutedEventArgs e)
        {
            StackPanel panel = sender as StackPanel;
            if (panel != null && panel.Tag != null)
            {
                try
                {
                    this.HomePagePivot.SelectedIndex = Int16.Parse(panel.Tag.ToString());
                }
                catch (FormatException) { }
            }
            if (HomePagePivot.SelectedIndex == 2)
            {
                 PopulateData("LastUsed");
            }
            else
                 PopulateData("Technora");
        }
        public async Task PopulateData(string type)
        {

            //<-----new changes for chinese---------------->
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            var denier = loader.GetString("lblDenier");
            var dtex = loader.GetString("lblDtex");
            //<--------end new changes----------->

            if (type == "LastUsed")
            {
                List_lastUsed.ItemsSource = LastItems.OrderBy(tt => tt.Name);
                //List_lastUsed.ItemsSource = LastItems;
            }
            else
            {

                string contents = String.Empty;

                //var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync("data.txt");

                var folder = Windows.ApplicationModel.Package.Current.InstalledLocation;
                if (type == "Twaron")
                {
                    var file = await folder.GetFileAsync("Twaron.js");

                    contents = await Windows.Storage.FileIO.ReadTextAsync(file);
                }

                else
                {


                    var file = await folder.GetFileAsync("Technora.js");

                    contents = await Windows.Storage.FileIO.ReadTextAsync(file);

                }


                //var path = Path.GetPathRoot("/data.js");

                var jData = JsonObject.Parse(contents);
                var jArray = jData.GetNamedArray("Material");
                List<Material> items = new List<Material>();
                foreach (var item in jArray)
                {
                    var itemObject = item.GetObject();
                    Material mate = new Material();

                    string itemID = itemObject.GetNamedString("ID");
                    string itemName = itemObject.GetNamedString("Name");
                    string itemStyle = itemObject.GetNamedString("Type");
                    string LinearDensity = itemObject.GetNamedString("LinearDensity");
                    string ActualLinearDensity = itemObject.GetNamedString("ActualLinearDensity");
                    string Selected = itemObject.GetNamedString("Selected");
                    if (Converters.UnitConverter.runninglengthLinerd == denier && OldValue.runninglengthLinerd == dtex)
                    {

                        mate.deniers = Math.Round((Convert.ToDouble(ActualLinearDensity) * .9), 0).ToString() +  denier;
                    }
                    mate.ID = Convert.ToInt16(itemID);                    
                    mate.Name = itemName + " " + itemStyle + ","+" " + LinearDensity +" "+dtex ;
                    if (mate.Name == oldColorMaterialL.name)
                        mate.Selected = "Red";
                    else
                        mate.Selected = Selected;

                    mate.Type = ActualLinearDensity;
           

                    items.Add(mate);


                }
                //List<Material> items = JsonConvert.DeserializeObject<List<Material>>(content);
                if (type == "Twaron")
                    lst_books.ItemsSource = items;
                else
                    lst_technora.ItemsSource = items;
            }
        }



        private void chkbx_Checked(object sender, RoutedEventArgs e)
        {
            //<-----new changes for chinese---------------->
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            var denier = loader.GetString("lblDenier");
            var dtex = loader.GetString("lblDtex");
            //<--------end new changes----------->
            RadioButton chk2 = (RadioButton)sender;

            Material mat = new Material();
            if (Converters.UnitConverter.runninglengthLinerd == denier && OldValue.runninglengthLinerd == dtex)
            {

                mat.deniers = Math.Round((Convert.ToDouble(chk2.Tag) * .9), 0).ToString() + denier;
            }
            mat.Name = chk2.Content.ToString();
            mat.Type = chk2.Tag.ToString();
            LastItems.Add(mat);
            if (Converters.UnitConverter.runninglengthLinerd == denier && OldValue.bobbinweightLinerd == dtex)
            {
                MainPage.materialBoxL.Text = Math.Round((Convert.ToDouble(chk2.Tag.ToString()) * .9),0).ToString();
            
            }
            else
              

            MainPage.materialBoxL.Text = chk2.Tag.ToString();

            MainPage.runningitemname.Text = chk2.Content.ToString();
            if(chk2.Content.ToString().Contains("Technora"))
                oldColorMaterialL.index = 1;
            else
                oldColorMaterialL.index = 0;

            oldColorMaterialL.name = chk2.Content.ToString();
            oldColorMaterialL.Color = true;
           

            chk2.IsChecked = true;



            ClosePopup();
        }

        private void chkbxlst_Checked(object sender, RoutedEventArgs e)
        {
            //<-----new changes for chinese---------------->
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            var denier = loader.GetString("lblDenier");
            var dtex = loader.GetString("lblDtex");
            //<--------end new changes----------->
            RadioButton chk2 = (RadioButton)sender;
            //   MainPage cs =new MainPage();
            //TextBox txt = (TextBox)cs.FindName("MaterialBox");
            //  txt.Text=chk2.Tag.ToString();

            Material mate = LastItems.Where(tt => tt.Name == chk2.Content.ToString()).FirstOrDefault();


            LastItems.Remove(mate);
            mate.Selected = "Red";
            LastItems.Add(mate);
            oldColorMaterialL.name = "";
            oldColorMaterialL.Color = false;
            oldColorMaterialL.index = 2;

            if (Converters.UnitConverter.bobbinweightLinerd == denier)
            {

                Material mt = new Material();
                MainPage.materialBox.Text = Math.Round((Convert.ToDouble(chk2.Tag.ToString()) * .9),0).ToString();
          
            }
            else
                MainPage.materialBox.Text = chk2.Tag.ToString();

            MainPage.runningitemname.Text = chk2.Content.ToString();
            //MainPage ps = new MainPage();
            //TextBlock TKS = (TextBlock)ps.FindName("LinearDensitytxt");

            //TextBlock bobbinT = (TextBlock)ps.FindName("boobinTube");
            //OldValue.bobbinweightLinerd = TKS.Text;
            //TextBlock bobbinL = (TextBlock)ps.FindName("bobbinRLength");
            //OldValue.bobbinweightLinerd = TKS.Text;
            //TextBlock runningT = (TextBlock)ps.FindName("runningTube");
            //OldValue.bobbinweightLinerd = TKS.Text;
            //TextBlock runningLL = (TextBlock)ps.FindName("runninglengthLinerd");
            //OldValue.bobbinweightLinerd = TKS.Text;
            //TextBlock runningBW = (TextBlock)ps.FindName("runningbobbinweight");
            //OldValue.bobbinweightLinerd = TKS.Text;


            ClosePopup();
        }


        /// <summary>
        /// function to closethe popup box
        /// </summary>
        private void ClosePopup()
        {
            Popup popupHelloWorld = this.Parent as Popup;
            popupHelloWorld.IsOpen = false;
            MainPage.isMenuOpen = false;
        }

        /// <summary>
        /// Event to close popup
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">RoutedEventArgs</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            ClosePopup();
        }

        public void ScaleFactor()
        {
            var scaleFactor = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;
            var WidthResolution = Window.Current.Bounds.Width * scaleFactor;
            var Heightresolution = Window.Current.Bounds.Height * scaleFactor;
            if (scaleFactor == 2.2)
            {
                TechnoraImg.Margin = new Thickness(0, 2, 0, 0);
                TwaronImag.Margin = new Thickness(0, 14.5, 0, 0);
                pivotcontent.Width = 420;
                pivotcontent.Height = 770;
            }
            else if (scaleFactor == 2.4)
            {
                TechnoraImg.Margin = new Thickness(0, 2, 0, 0);
                TwaronImag.Margin = new Thickness(0, 14.5, 0, 0);
                pivotcontent.Width = 380;
                pivotcontent.Height = 700;
            }
            else if (scaleFactor == 1.8)
            {
                TechnoraImg.Margin = new Thickness(0, 15, 0, 0);
                TwaronImag.Margin = new Thickness(0, 25, 0, 0);
                ImgLastUsed.Margin = new Thickness(0, 26, 0, 0);
                pivotcontent.Width = 320;
                pivotcontent.Height = 600;
            }
            else if (scaleFactor == 2.0)
            {
                pivotcontent.Width = 315;
                pivotcontent.Height = 545;
            }
            else if (scaleFactor == 1.2 && WidthResolution != 540.0 && Heightresolution != 960.0)
            {
                pivotcontent.Width = 330;
                pivotcontent.Height = 550;
            }
            else if (scaleFactor == 1.2 && WidthResolution == 540.0 && Heightresolution == 960.0)
            {
                TechnoraImg.Margin = new Thickness(0, 2.5, 0, 0);
                TwaronImag.Margin = new Thickness(0, 14.5, 0, 0);
                pivotcontent.Width = 380;
                pivotcontent.Height = 660;

            }     
            //else if (scaleFactor == 1.2)
            //{
            //    pivotcontent.Width = 330;
            //    pivotcontent.Height = 570;
            //}
            else if (scaleFactor == 1.6)
            {
                TechnoraImg.Margin = new Thickness(0, 3, 0, 0);
                TwaronImag.Margin = new Thickness(0, 14.5, 0, 0);
                pivotcontent.Width = 370;
                pivotcontent.Height = 689;
            }
            else if (scaleFactor == 3.4)
            {
                pivotcontent.Width = 350;
                pivotcontent.Height = 655;
            }
            else
            {
                getScreenInfo();
            }
        }

        public void getScreenInfo()
        {
            var height = Window.Current.Bounds.Height * (0.73 - 0.008);
            var width = Window.Current.Bounds.Width*0.8;
            //var Actualheigt = height - 540;

            //var lgt = height - Actualheigt;
            pivotcontent.Width = width;
            pivotcontent.Height = height;
        }
    }
}
