﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using RL_Calculator;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Popups;
using System.Text;
using Windows.ApplicationModel.DataTransfer;
using Windows.Graphics.Display;
using System.Windows;
using input = System.Windows.Input;
using System.Windows.Input;
using Windows.UI.ViewManagement;
using Windows.UI.Core;
using Windows.System;
using Windows.ApplicationModel.Resources.Core;
using System.Globalization;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace RL_Calculator
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.runninglength,bobbinweight
    /// </summary>
    public sealed partial class MainPage : Page
    {
         string lang = new MessageDialog(Windows.System.UserProfile.GlobalizationPreferences.Languages[0]).Content;
        
        Double offset = 0;
        //StringBuilder MyStringBuilder = new StringBuilder("\n'You are receiving your running length calculation done by the Running Length App from Teijin Aramid.'\n\nYour calculation:\n");
        StringBuilder MyStringBuilder = new StringBuilder("\n" +new Windows.ApplicationModel.Resources.ResourceLoader().GetString("shareText1") + "\n\n"+new Windows.ApplicationModel.Resources.ResourceLoader().GetString("shareText2")+"\n");

        public static bool? isMenuOpen;
        string val = "a";
        public bool state;
        public static Image LeftmenuTapped, Leftmenu1Tapped;
        public static TextBox materialBox, materialBoxL, tubeBox, tubeL, TBW, RRL;
        public static TextBlock bobbinweightLinerd, runninglengthLinerd, boobinTube, runningTube, bobbinRLength, runningbobbinweight, Totalbobin, totalrunning, bobinitemname, runningitemname, runningtubitem, bobintubitem;
        //public int count=0;
        private DataTransferManager datatransferManager;
       public  MainPage()
        {

            this.InitializeComponent();
            //SetControls();


            var cult = CultureInfo.CurrentCulture.ToString();
            if (cult.Contains("zh"))
            {
               lblrrl.FontSize=20;
               lblbobbinwttb1.FontSize = 20;
            }
        
            ScaleFactor();
            UnitChangeDynamicaly();
            state = false;
         //   DrawerLayout.InitializeDrawerLayout();
            isMenuOpen = true;
            //var inputPane = InputPane.GetForCurrentView();
            //inputPane.Showing += OnShowingInputPane;
            LeftmenuTapped = (Image)FindName("LeftButtonTapped");
           // Leftmenu1Tapped = (Image)FindName("LeftButton1Tapped");

            materialBox = (TextBox)FindName("MaterialBox");
            tubeBox = (TextBox)FindName("TubeBox");
            materialBoxL = (TextBox)FindName("materialL");
            tubeL = (TextBox)FindName("tubeBoxL");
            TBW = (TextBox)FindName("TotalBobbinWeight");
            RRL = (TextBox)FindName("BobbinRunningLength");

            boobinTube = (TextBlock)FindName("tubewtxt");
            bobbinRLength = (TextBlock)FindName("bobinlength");
            bobbinweightLinerd = (TextBlock)FindName("LinearDensitytxt");


            runninglengthLinerd = (TextBlock)FindName("Densitytxt");
            runningTube = (TextBlock)FindName("Tweighttxt");
            runningbobbinweight = (TextBlock)FindName("Bobinweighttxt");
            Totalbobin = (TextBlock)FindName("resultbobbin");
            totalrunning = (TextBlock)FindName("RunnigResult");

            runningitemname = (TextBlock)FindName("matitemname");
            bobinitemname = (TextBlock)FindName("bobmatitemname");
            runningtubitem = (TextBlock)FindName("runningtubitemname");
            bobintubitem = (TextBlock)FindName("bobintubitemname");

           this.NavigationCacheMode = NavigationCacheMode.Disabled;
            //var _Frame = Window.Current.Content as Frame;
            //_Frame.Navigate(_Frame.Content.GetType());
            //_Frame.GoBack();

        }

        private async void OnShowingInputPane(InputPane sender, InputPaneVisibilityEventArgs args)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                var parentScrollViewer = FindParent<ScrollViewer>(this);
                parentScrollViewer.VerticalScrollMode = ScrollMode.Auto;
                offset = 580 - args.OccludedRect.Top;
                parentScrollViewer.ChangeView(null, offset, null, true);
                parentScrollViewer.UpdateLayout();
            });
        }
        //<----- material box got focus---------->
        private void MaterialBox_GotFocus(object sender, RoutedEventArgs e)
        {
  
            scitem1.Height = pivotcontent.ActualHeight-350;

            scitem1.VerticalAlignment = VerticalAlignment.Top;
           //   scitem1.ChangeView(null,ActualHeight,null);
            scitem1.ScrollToVerticalOffset(MaterialBox.ActualHeight);
            //var parentScrollViewer = FindParent<ScrollViewer>(this);
            //parentScrollViewer.VerticalScrollMode = ScrollMode.Auto;
            //parentScrollViewer.ChangeView(null, offset, null, true);
            //parentScrollViewer.UpdateLayout();
            bobmatitemname.Text = "";
        }
        //<----- material box lost focus---------->
        private void MaterialBox_LostFocus(object sender, RoutedEventArgs e)
        {

            scitem1.Height = pivotcontent.ActualHeight;

            scitem1.VerticalAlignment = VerticalAlignment.Top;
            //var parentScrollViewer = FindParent<ScrollViewer>(this);
            //parentScrollViewer.VerticalScrollMode = ScrollMode.Auto;
            //parentScrollViewer.ChangeView(null, offset, null, true);
            //parentScrollViewer.UpdateLayout();
            if (MaterialBox.Text == "")
            {
                resultbobbin.Text = "";
            }
            else
            {
                BobbinResult();
            }

            isMenuOpen = false;
        }

        //<----- tube box got focus---------->
        private void tubewtxt_GotFocus(object sender, RoutedEventArgs e)
        {


            scitem1.Height = pivotcontent.ActualHeight-350;

            scitem1.VerticalAlignment = VerticalAlignment.Top;
            scitem1.ScrollToVerticalOffset(tubewtxt.ActualHeight);
            //var parentScrollViewer = FindParent<ScrollViewer>(this);
            //parentScrollViewer.VerticalScrollMode = ScrollMode.Auto;
            //parentScrollViewer.ChangeView(null, offset, null, true);
            //parentScrollViewer.UpdateLayout();

            bobintubitemname.Text = "";
        }
        //<----- tube box tubewtxt_LostFocus focus---------->
        private void tubewtxt_LostFocus(object sender, RoutedEventArgs e)
        {

         


            scitem1.Height = pivotcontent.ActualHeight ;

            scitem1.VerticalAlignment = VerticalAlignment.Top;

          
            //var parentScrollViewer = FindParent<ScrollViewer>(this);
            //parentScrollViewer.VerticalScrollMode = ScrollMode.Auto;
            //parentScrollViewer.ChangeView(null, offset, null, true);
            //parentScrollViewer.UpdateLayout();
            if (TubeBox.Text == "")
            {
                resultbobbin.Text = "";
            }
            else
            {
                BobbinResult();
            }
            isMenuOpen = false;
        }
        //<----- BobbinRunningLength_GotFocus box got focus---------->
        private void BobbinRunningLength_GotFocus(object sender, RoutedEventArgs e)
        {

            if (BobbinRunningLength.Text != "")
                BobbinRunningLength.Text = "";

            scitem1.Height = pivotcontent.ActualHeight - 350;

            scitem1.VerticalAlignment = VerticalAlignment.Top;
            scitem1.ScrollToVerticalOffset(BobbinRunningLength.ActualHeight);
            //var parentScrollViewer = FindParent<ScrollViewer>(this);
            //parentScrollViewer.VerticalScrollMode = ScrollMode.Auto;
            //parentScrollViewer.ChangeView(null, offset, null, true);
            //parentScrollViewer.UpdateLayout();
        }
        

        //private void BobbinRunningLength_LostFocus(object sender, RoutedEventArgs e)
        //{




        //    scitem1.Height = pivotcontent.ActualHeight ;

        //    scitem1.VerticalAlignment = VerticalAlignment.Top;

        //    //var parentScrollViewer = FindParent<ScrollViewer>(this);
        //    //parentScrollViewer.VerticalScrollMode = ScrollMode.Auto;
        //    //parentScrollViewer.ChangeView(null, offset, null, true);
        //    //parentScrollViewer.UpdateLayout();
        //}



        //<----- materialL box got focus---------->
        private void materialL_GotFocus(object sender, RoutedEventArgs e)
        {

            scitem2.Height = pivotcontent2.ActualHeight - 350;

            scitem2.VerticalAlignment = VerticalAlignment.Top;
            scitem2.ScrollToVerticalOffset(materialL.ActualHeight);
            //var parentScrollViewer = FindParent<ScrollViewer>(this);
            //parentScrollViewer.VerticalScrollMode = ScrollMode.Auto;
            //parentScrollViewer.ChangeView(null, offset, null, true);
            //parentScrollViewer.UpdateLayout();

            matitemname.Text = "";

        }
        //<----- materialL box materialL_LostFocus---------->
        private void materialL_LostFocus(object sender, RoutedEventArgs e)
        {


            scitem2.Height = pivotcontent2.ActualHeight;

            scitem2.VerticalAlignment = VerticalAlignment.Top;
            //var parentScrollViewer = FindParent<ScrollViewer>(this);
            //parentScrollViewer.VerticalScrollMode = ScrollMode.Auto;
            //parentScrollViewer.ChangeView(null, offset, null, true);
            //parentScrollViewer.UpdateLayout();
            if (materialL.Text == "")
            {
                RunnigResult.Text = "";
            }
            else
            {
                RunningResult();
            }
            isMenuOpen = false;
        }

        //<----- tubeL box got focus---------->
        private void tubeBoxL_GotFocus(object sender, RoutedEventArgs e)
        {


            scitem2.Height = pivotcontent2.ActualHeight - 350;

            scitem2.VerticalAlignment = VerticalAlignment.Top;
            scitem2.ScrollToVerticalOffset(tubeBoxL.ActualHeight);
            //var parentScrollViewer = FindParent<ScrollViewer>(this);
            //parentScrollViewer.VerticalScrollMode = ScrollMode.Auto;
            //parentScrollViewer.ChangeView(null, offset, null, true);
            //parentScrollViewer.UpdateLayout();
            runningtubitemname.Text = "";
        }
        //<----- tubeL box tubewtxt_LostFocus focus---------->
        private void tubeBoxL_LostFocus(object sender, RoutedEventArgs e)
        {




            scitem2.Height = pivotcontent2.ActualHeight;

            scitem2.VerticalAlignment = VerticalAlignment.Top;


            //var parentScrollViewer = FindParent<ScrollViewer>(this);
            //parentScrollViewer.VerticalScrollMode = ScrollMode.Auto;
            //parentScrollViewer.ChangeView(null, offset, null, true);
            //parentScrollViewer.UpdateLayout();
            if (tubeBoxL.Text == "")
            {
                RunnigResult.Text = "";
            }
            else
            {
                RunningResult();
            }
            isMenuOpen = false;
        }




        //----- total bobbin weight got focus---------------
        private void TotalBobbinWeight_GotFocus(object sender, RoutedEventArgs e)
        {

            if (TotalBobbinWeight.Text != "")
                TotalBobbinWeight.Text = "";
            scitem2.Height = pivotcontent.ActualHeight - 350;

            scitem2.VerticalAlignment = VerticalAlignment.Top;
            scitem2.ScrollToVerticalOffset(TotalBobbinWeight.ActualHeight);
            //var parentScrollViewer = FindParent<ScrollViewer>(this);
            //parentScrollViewer.VerticalScrollMode = ScrollMode.Auto;
            //parentScrollViewer.ChangeView(null, offset, null, true);
            //parentScrollViewer.UpdateLayout();
        }

        private void tubewtxt_Tapped(object sender, TappedRoutedEventArgs e)
        {
            offset = e.GetPosition(borderTube).Y - 10;
        }
        public static T FindParent<T>(FrameworkElement reference)
           where T : FrameworkElement
        {
            FrameworkElement parent = reference;
            while (parent != null)
            {
                parent = parent.Parent as FrameworkElement;

                var rc = parent as T;
                if (rc != null)
                {
                    return rc;
                }
            }
            return null;
        }
        //---------------Navigation-----------
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;
            datatransferManager = DataTransferManager.GetForCurrentView();
            datatransferManager.DataRequested += new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.OndataRequested);
            try
            {



                slidein.GoToMenuState(ActiveState.Right);
                {

                }
               // this.NavigationCacheMode = NavigationCacheMode.Disabled;
            }

            catch
            { }
          
            
        }


        private void OndataRequested(DataTransferManager sender, DataRequestedEventArgs e)
        {


            //var request = e.Request;
            //request.Data.Properties.Title = "'You are receiving your running length calculation done by the Running Length App from Teijin Aramid.'";
            //request.Data.Properties.Description = "Share your result";
            //request.Data.SetText(MyStringBuilder.ToString());





            var request = e.Request;

            request.Data.Properties.Title = "" + new Windows.ApplicationModel.Resources.ResourceLoader().GetString("shareText1") + "";
            request.Data.Properties.Description = "Share your result";
            request.Data.SetText(MyStringBuilder.ToString());

        }
        //private void OnNavigatedFrom (NavigationEventArgs e)
        //{
        //    slidein.GoToMenuState(ActiveState.Both);
        //}

        //----------------Tab Button Click Event----------------->
        private void TabButtonClick(object sender, TappedRoutedEventArgs e)
        {
            //if(Convert.ToBoolean(HomePagePivot.SelectedIndex=0))
            //{
            //    isMenuOpen = true;
            //}
            //else
            //    isMenuOpen = false;
            StackPanel panel = sender as StackPanel;
            if (panel != null && panel.Tag != null)
            {
                try
                {
                    this.HomePagePivot.SelectedIndex = Int16.Parse(panel.Tag.ToString());
                    isMenuOpen = false;
                }
                catch (FormatException) { }
            }
           // var TXT = isMenuOpen;
           // if (TXT==false)
           //{
           //    isMenuOpen = true;
           //}
           //else
           //    isMenuOpen = false;
        }

       //-------------Pop UP material=------------->
        private void PopUpMaterial_Click(object sender, RoutedEventArgs e)
        {

            Popup PopUpMaterial;
            PopUpMaterial = new Popup();
            //Popup mypopup = new Popup();
            //mypopup.Child  = new PopUpMaterial();
            //mypopup.IsOpen = true;
            //set the content to be hosted inside the popup.
            PopUpMaterial.Child =
                new PopUpMaterial();

            PopUpMaterial.IsOpen = true;

            PopUpMaterial.VerticalOffset = 20;// distance of the popup from the top
            //PopUpMaterial.HorizontalOffset = 0;// distance of the popup from the left 
            PopUpMaterial.Closed += (s1, e1) =>
            {
                // Add you code here to do something
                // when the Popup is closed



            };
            isMenuOpen = true;
        }



        public string callback(string s)
        {

            return s;

        }
        //Pop Up Tube-------------------
        private void PopUpTube_Click(object sender, RoutedEventArgs e)
        {
            Popup PopUpTube;
            PopUpTube = new Popup();

            PopUpTube.Child =
                new PopUpTube();

            PopUpTube.IsOpen = true;
            isMenuOpen = true;
            //PopUpTube.VerticalOffset = 30;// distance of the popup from the top
            // distance of the popup from the left 

        }


        //Pop Up Material L---------------------->
        private void PopUpMaterialL_Click(object sender, RoutedEventArgs e)
        {
            Popup PopUpMaterialL;
            PopUpMaterialL = new Popup();

            PopUpMaterialL.Child =
                new PopUpMaterialL();

            PopUpMaterialL.IsOpen = true;
            isMenuOpen = true;
            //PopUpMaterialL.VerticalOffset = 30;// distance of the popup from the top
            //PopUpMaterialL.HorizontalOffset = 0;// distance of the popup from the left 
        }
        //__________--Pop Up Tube L----------------->
        private void PopUpTubeL_Click(object sender, RoutedEventArgs e)
        {
            Popup PopUpTubeL;
            PopUpTubeL = new Popup();
            PopUpTubeL.Child =
                new PopUpTubeL();



            PopUpTubeL.IsOpen = true;

            //PopUpTubeL.VerticalOffset = 30;// distance of the popup from the top
            PopUpTubeL.HorizontalOffset = 0;// distance of the popup from the left 
            isMenuOpen = true;
        }
      //-------------side menu open close-------------->
        private void LeftButtonTapped_Tapped(object sender, TappedRoutedEventArgs e)
        {
            
            //var state = Convert.ToBoolean(Share_btn.Visibility=Visibility.Visible);
            //if (state!=true)
            //{
            //    slidein.GoToMenuState(ActiveState.Right);
            //    LeftmenuGrid.Visibility = Visibility.Collapsed;
            //}
            //else
            //    slidein.GoToMenuState(ActiveState.Left);
            //LeftmenuGrid.Visibility = Visibility.Visible;
            //this.LeftmenuGrid.GotFocus += (s1,e1)=>
            //    {
            //        Share_btn.Visibility = Visibility.Collapsed;
            //    };
              
            //var state = slidein.MenuState.ToString();
            //if(state=="Both" state=="right")
            //{
            //    slidein.GoToMenuState(ActiveState.Left);
            //}
            //else
            //    slidein.GoToMenuState(ActiveState.Right);
          //if (Convert.ToBoolean(x))
          //  {
          //      slidein.GoToMenuState(ActiveState.Right);
          //  }
          //  else
          //      slidein.GoToMenuState(ActiveState.Left);
            
          // var content= slidein.RightMenuContent;
          //  //var state = Convert.ToBoolean(slidein.MenuState.ToString());  
          // if (Convert.ToBoolean( content=true)) 
          //{
          //    slidein.GoToMenuState(ActiveState.Left);
          //    content = false;
          //  }
          //else
          //  {
          //      slidein.GoToMenuState(ActiveState.Left);
          //      content = true;
          //  }
              
                        
            //LeftButtonTapped.Visibility = Visibility.Collapsed;
            //LeftButton1Tapped.Visibility = Visibility.Visible;
            //if (DrawerLayout.IsDrawerOpen)
            //{
            //    DrawerLayout.CloseDrawer();
            //    slidein.GoToMenuState(ActiveState.Right);
            //    {

            //    }
            //}
            //else
            //    DrawerLayout.OpenDrawer();
            //slidein.GoToMenuState(ActiveState.Left);
            //{ }

            if (Convert.ToBoolean(isMenuOpen))
            {
                slidein.GoToMenuState(ActiveState.Right);
                {

                }
                isMenuOpen = false;
            }
            else
            {
                slidein.GoToMenuState(ActiveState.Left);
                {

                }
                isMenuOpen = true;
            }
          
        }
        //-------------- Side menu Home Button----------->
        private void HomeButtonClick(object sender, TappedRoutedEventArgs e)
        {
            //LeftButtonTapped.Visibility = Visibility.Visible;
            //LeftButton1Tapped.Visibility = Visibility.Collapsed;

            try
            {

                LinearDensitytxt.Text = Converters.UnitConverter.bobbinweightLinerd;
                //slidein.GoToMenuState(ActiveState.Content);
                //slidein.GoToMenuState(ActiveState.Right);

            }
            catch
            {

            }

            slidein.GoToMenuState(ActiveState.Right);
            {

            }
            isMenuOpen = true;
            //Window.Current.Activate();

        }
        //-----------------Side menu Unit Button-------------->
        private void unitsButtonClick(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                Frame.Navigate(typeof(Units));
            }
            catch(Exception Ex)
            {
                var dialog = new MessageDialog(Ex.Message);
                dialog.ShowAsync();
            }
        }
        //=------------------side menu info button-------------->
        private void InfoButtonClick(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Info));
        }


        //------------------- Share button Click-------------------->
        private void sharebtn_clicked(object sender, TappedRoutedEventArgs e)
        {
            //MyStringBuilder.Clear();
 
            if ((RunnigResult.Text == "" || TotalBobbinWeight.Text == "" || tubeBoxL.Text == ""  || materialL.Text == "" )&&( resultbobbin.Text == "" || BobbinRunningLength.Text == "" || TubeBox.Text == "" || MaterialBox.Text==""))
            {
                var dialog = new MessageDialog("Teijin Aramid\n\n" + new Windows.ApplicationModel.Resources.ResourceLoader().GetString("errorNoResultToShare") + "");
                dialog.ShowAsync();
               // isMenuOpen = true;
            }
            else
            {
                StringBuilder strgbldr = new StringBuilder();

                if (resultbobbin.Text != "")
                {
                    strgbldr.Append(""+new Windows.ApplicationModel.Resources.ResourceLoader().GetString("lblLinearDensityShare") +""+ " " + MaterialBox.Text + " " + LinearDensitytxt.Text + "\n");
                    strgbldr.Append("" + new Windows.ApplicationModel.Resources.ResourceLoader().GetString("lblWeightUnit") + "" + " " + TubeBox.Text + " " + tubewtxt.Text + "\n");
                    strgbldr.Append("" + new Windows.ApplicationModel.Resources.ResourceLoader().GetString("lblRequiredRunningLengthShare") + "" + " " + BobbinRunningLength.Text + " " + bobinlength.Text + "\n");
                    strgbldr.Append("" + new Windows.ApplicationModel.Resources.ResourceLoader().GetString("lblCalculatedMinBobbinWeight") + "" + " " + resultbobbin.Text + "\n" + "\n");

                }

                if (RunnigResult.Text != "")
                {
                    strgbldr.Append("" + new Windows.ApplicationModel.Resources.ResourceLoader().GetString("lblLinearDensityShare") + "" + " " + materialL.Text + " " + Densitytxt.Text + "\n");
                    strgbldr.Append("" + new Windows.ApplicationModel.Resources.ResourceLoader().GetString("lblWeightUnit") + "" + " " + tubeBoxL.Text + " " + Tweighttxt.Text + "\n");
                    strgbldr.Append("" + new Windows.ApplicationModel.Resources.ResourceLoader().GetString("calculateWeightTabLabelforsharing") + "" + " " + TotalBobbinWeight.Text + " " + Bobinweighttxt.Text + "\n");
                    strgbldr.Append("" + new Windows.ApplicationModel.Resources.ResourceLoader().GetString("lblCalculatedYarnRunningLengthOnBobbin") + "" + " " + RunnigResult.Text + "\n" + "\n");
                }
                MyStringBuilder.Replace("" + strgbldr + "", "");
                MyStringBuilder.Append(strgbldr);
                //this.Frame.Navigate(typeof(PopUpShare));
                DataTransferManager.ShowShareUI();
               // isMenuOpen = true;
            }
           isMenuOpen = false;

        }

//--------------------------------start--methods for finding result----------------------------->
        public void BobbinResult()
        {
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            var gr = loader.GetString("lblShortGram");
            var m = loader.GetString("lblShortMeter");
            var ft = loader.GetString("lblShortFeet");
            var yd = loader.GetString("lblShortYards");
            var kg = loader.GetString("lblShortKilogram");
            var km = loader.GetString("lblShortKilometers");

            try
            {


                if (tubewtxt.Text == gr || tubewtxt.Text == m || tubewtxt.Text == ft || tubewtxt.Text == yd)
                    resultbobbin.Text = (Math.Round(((decimal.Parse(BobbinRunningLength.Text) * decimal.Parse(MaterialBox.Text)) / 10000) + decimal.Parse(TubeBox.Text), 0)).ToString() + " " + tubewtxt.Text;
                else if (tubewtxt.Text == kg || tubewtxt.Text == km)

                    resultbobbin.Text = (Math.Round(((decimal.Parse(BobbinRunningLength.Text) * decimal.Parse(MaterialBox.Text)) / 10000) + decimal.Parse(TubeBox.Text), 3)).ToString() + " " + tubewtxt.Text;
                else
                    resultbobbin.Text = (Math.Round(((decimal.Parse(BobbinRunningLength.Text) * decimal.Parse(MaterialBox.Text)) / 10000) + decimal.Parse(TubeBox.Text), 2)).ToString() + " " + tubewtxt.Text;


            }
            catch { }
        }


        public void RunningResult()
        {
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            var gr = loader.GetString("lblShortGram");
            var m = loader.GetString("lblShortMeter");
            var ft = loader.GetString("lblShortFeet");
            var yd = loader.GetString("lblShortYards");
            var kg = loader.GetString("lblShortKilogram");
            var km = loader.GetString("lblShortKilometers");

            try
            {

                if ((decimal.Parse(TotalBobbinWeight.Text) > decimal.Parse(tubeBoxL.Text)))
                {
                    //var dialog = new MessageDialog("Your message here");
                    //await() dialog.ShowAsync();

                    if (bobinlength.Text == gr || bobinlength.Text == m || bobinlength.Text == ft || bobinlength.Text == yd)
                        RunnigResult.Text = (Math.Round(((decimal.Parse(TotalBobbinWeight.Text) - decimal.Parse(tubeBoxL.Text)) * 10000) / Decimal.Parse(materialL.Text), 0)).ToString() + " " + bobinlength.Text;
                    else if (bobinlength.Text == kg || bobinlength.Text == km)

                        RunnigResult.Text = (Math.Round(((decimal.Parse(TotalBobbinWeight.Text) - decimal.Parse(tubeBoxL.Text)) * 10000) / Decimal.Parse(materialL.Text), 3)).ToString() + " " + bobinlength.Text;
                    else
                        RunnigResult.Text = (Math.Round(((decimal.Parse(TotalBobbinWeight.Text) - decimal.Parse(tubeBoxL.Text)) * 10000) / Decimal.Parse(materialL.Text), 2)).ToString() + " " + bobinlength.Text;



                }
                else if ((decimal.Parse(TotalBobbinWeight.Text) <= decimal.Parse(tubeBoxL.Text)))
                {





                    var dialog = new MessageDialog("Teijin Aramid \n\n\n The Value for 'Total bobbin weight' cannot be less than or equal to the value for 'weight tube'");
                    dialog.ShowAsync();
                    TotalBobbinWeight.Text = "";
                }
            }
            catch
            { }
        }

        //--------------end of result methods------------------------------------>
// ---------------------- start-result calculation--------------------------->


        private void BobbinRunningLength_LostFocus(object sender, RoutedEventArgs e)
       
        {

              scitem1.Height = pivotcontent.ActualHeight ;

           scitem1.VerticalAlignment = VerticalAlignment.Top;
            try
            {


                if (BobbinRunningLength.Text == "")
                {
                    resultbobbin.Text = "";
                }
                else
                {
                    BobbinResult();
                }

                isMenuOpen = false;

                //if (tubewtxt.Text=="gr" || tubewtxt.Text=="m"  || tubewtxt.Text=="ft" || tubewtxt.Text=="yd"  )
                //resultbobbin.Text = (Math.Round(((decimal.Parse(BobbinRunningLength.Text) * decimal.Parse(MaterialBox.Text)) / 10000) + decimal.Parse(TubeBox.Text),0)).ToString() + " " + tubewtxt.Text;
                //else if(tubewtxt.Text=="kg" || tubewtxt.Text=="km")

                // resultbobbin.Text = (Math.Round(((decimal.Parse(BobbinRunningLength.Text) * decimal.Parse(MaterialBox.Text)) / 10000) + decimal.Parse(TubeBox.Text), 3)).ToString() + " " + tubewtxt.Text;
                //else
                //    resultbobbin.Text = (Math.Round(((decimal.Parse(BobbinRunningLength.Text) * decimal.Parse(MaterialBox.Text)) / 10000) + decimal.Parse(TubeBox.Text), 2)).ToString() + " " + tubewtxt.Text;

            
            }
            catch { }

        }

        private void TotalBobbinWeight_LostFocus(object sender, RoutedEventArgs e)
        {

           
          
 

            try
            {
                if (TotalBobbinWeight.Text == "")
                {
                    RunnigResult.Text = "";
                }
                else
                {
                    RunningResult();
                }

                isMenuOpen = false;
            //    if ((decimal.Parse(TotalBobbinWeight.Text) > decimal.Parse(tubeBoxL.Text)))
            //    {
            //        //var dialog = new MessageDialog("Your message here");
            //        //await() dialog.ShowAsync();

            //        if (bobinlength.Text == "gr" || bobinlength.Text == "m" || bobinlength.Text == "ft" || bobinlength.Text == "yd")
            //            RunnigResult.Text = (Math.Round(((decimal.Parse(TotalBobbinWeight.Text) - decimal.Parse(tubeBoxL.Text)) * 10000) / Decimal.Parse(materialL.Text), 0)).ToString() + " " + bobinlength.Text;
            //        else if (bobinlength.Text == "kg" || bobinlength.Text == "km")

            //            RunnigResult.Text = (Math.Round(((decimal.Parse(TotalBobbinWeight.Text) - decimal.Parse(tubeBoxL.Text)) * 10000) / Decimal.Parse(materialL.Text), 3)).ToString() + " " + bobinlength.Text;
            //        else
            //            RunnigResult.Text = (Math.Round(((decimal.Parse(TotalBobbinWeight.Text) - decimal.Parse(tubeBoxL.Text)) * 10000) / Decimal.Parse(materialL.Text), 2)).ToString() + " " + bobinlength.Text;



            //    }
            //    else if ((decimal.Parse(TotalBobbinWeight.Text) <= decimal.Parse(tubeBoxL.Text)))
            //    {





            //        var dialog = new MessageDialog("Teijin Aramid \n\n\n The Value for 'Total bobbin weight' cannot be less than or equal to the value for 'weight tube'");
            //        dialog.ShowAsync();
            //        TotalBobbinWeight.Text = "";
            //    }
            }
            catch
            { }
            finally
            {
                scitem2.Height = pivotcontent2.ActualHeight;
                scitem1.Height = pivotcontent.ActualHeight;
                //scitem1.VerticalAlignment = VerticalAlignment.Top;
            
            }

        }


// ---------------------------end of result calculation----------------------------------->

        //private void LeftButton1Tapped_Tapped(object sender, TappedRoutedEventArgs e)
        //{
        //    slidein.GoToMenuState(ActiveState.Right);

        //    LeftButtonTapped.Visibility = Visibility.Visible;
        //    LeftButton1Tapped.Visibility = Visibility.Collapsed;
        //}



        //--------------------------- Static Device Scaling ------------------->
        public void ScaleFactor()
        {
            //pivotcontent2.Height = 520;
            var scaleFactor = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;
            var WidthResolution=Window.Current.Bounds.Width* scaleFactor;
            var Heightresolution = Window.Current.Bounds.Height * scaleFactor;

            if (scaleFactor == 2.2)
            {
                pivotcontent.Height = 645;

            }
            else if (scaleFactor == 2.4)
            {
                pivotcontent.Height = 580;
            }

            else  if (scaleFactor == 1.8)
            {
                pivotcontent.Height = 440;
            }

            else if (scaleFactor == 2.0)
            {
                pivotcontent.Height = 435;
            }

            else if (scaleFactor == 1.2 && WidthResolution != 540.0 && Heightresolution != 960.0)
            {
                pivotcontent.Height = 455;
            }
            else if (scaleFactor == 1.2 && WidthResolution == 540.0 && Heightresolution == 960.0)
            {
                pivotcontent.Height = 530;

            }

            else if (scaleFactor == 1.6 && WidthResolution==720.0  && Heightresolution==1280.0)
            {
                pivotcontent.Height = 530;
               
            }
            else if (scaleFactor == 1.6 && WidthResolution != 720.0 && Heightresolution != 1280.0)
            {
                pivotcontent.Height = 580;

            }

            else if (scaleFactor == 3.4)
            {
                pivotcontent.Height = 540;

            }
            else
            {
                getScreenInfo();
            }
               

        }



        
      

      

      


        //private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        //{
        //    Control box = sender as Control;
        //    StackPanel parentStack = box.Parent as StackPanel;
        //    currentEditingRow = parentStack.Children.IndexOf(box);
        //}

        //private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        //{
        //    currentEditingRow = -1;
        //}
        //private void TextBlock_Tap(object sender, TappedRoutedEventArgs e)
        //{
        //    Border block = sender as Border;



        //    Grid parentStack = block.Parent as Grid;
        //    int textBlockIndex = parentStack.Children.IndexOf(block);
        //    Control textBox = null;
        //    if (textBlockIndex == currentEditingRow - 1)
        //    {
        //        textBox = parentStack.Children.ElementAtOrDefault(textBlockIndex - 1) as Control;
        //    }
        //    else
        //    {
        //        textBox = parentStack.Children.ElementAtOrDefault(textBlockIndex + 1) as Control;
        //    }
        //    if (textBox != null)
        //        textBox.Focus(FocusState.Keyboard);
        //}



        public  void getScreenInfo()
        {
            var height = Window.Current.Bounds.Height*(0.73-0.008);
            var width = Window.Current.Bounds.Width;
            //var Actualheigt = height - 540;

            //var lgt = height - Actualheigt;

            pivotcontent.Height = height;
            pivotcontent.Width = width;

          
        






            //var width = Window.Current.Bounds.Width * (int)DisplayProperties.ResolutionScale / 100;
            //var height = Window.Current.Bounds.Height * (int)DisplayProperties.ResolutionScale / 100;

            //var dpiy = DisplayInformation.GetForCurrentView().RawDpiY;
            //var dpix = DisplayInformation.GetForCurrentView().RawDpiX;

            //var screenDiagonal = Math.Sqrt(Math.Pow(width / dpix, 2) +
            //            Math.Pow(height / dpiy, 2));

            //return screenDiagonal.ToString();
            //pivotcontent.MaxWidth = width;
            //pivotcontent.MaxHeight = height;
        
        //    double dpix = -1.01;
        //    double screensize = -1.01;
        //    double dpiy = -1.01;
        //    Size res;
        //    try
        //    {
            //dpix = (double)DeviceExtendedProperties.GetValue("RawDpiX");
            //dpiy = (double)DeviceExtendedProperties.GetValue("RawDpiY");
            //res = (Size)DeviceExtendedProperties.GetValue("PhysicalScreenResolution");
            //screensize = Math.Sqrt(Math.Pow(res.Width / dpix, 2) + Math.Pow(res.Height / dpiy, 2));
        //    }
        //    catch (Exception e)
        //    {
        //    }
        }
        //------------- validation for textboxex----------------------------------->
        private void materialL_TextChanged(object sender, TextChangedEventArgs e)
        {
            
         
            if (materialL.Text == "")
            {
                RunnigResult.Text = "";
            }
            else
            {
                RunningResult();
            }
        }

        private void tubeBoxL_TextChanged(object sender, TextChangedEventArgs e)
        {
           
            if (tubeBoxL.Text == "")
            {
                RunnigResult.Text = "";
            }
            else
            {
                RunningResult();
            }
        }

        private  void MaterialBox_TextChanged(object sender, TextChangedEventArgs e)
        {
           
            if (MaterialBox.Text == "")
            {
                resultbobbin.Text = "";
            }
            else
            {
                BobbinResult();
            }
        }

        private void TubeBox_TextChanged(object sender, TextChangedEventArgs e)
        {
          
            if (TubeBox.Text == "")
            {
                resultbobbin.Text = "";
            }
            else
            {
                BobbinResult();
            }
        }
        //<-------------------End of validation--------------->
        //<------Keyboard remove on enter key press-------------------->
        private void TotalBobbinWeight_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                this.Focus(FocusState.Programmatic);
            }
            MaskNumericInput((TextBox)sender, false);
        }

        private void materialL_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                this.Focus(FocusState.Programmatic);
            }
            MaskNumericInput((TextBox)sender, false);
        }



        private void tubeBoxL_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                this.Focus(FocusState.Programmatic);
            }
            MaskNumericInput((TextBox)sender, false);
        }

        private void MaterialBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                this.Focus(FocusState.Programmatic);
            }
            MaskNumericInput((TextBox)sender, false);
        }

        private void TubeBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                this.Focus(FocusState.Programmatic);
            }
            MaskNumericInput((TextBox)sender, false);
        }

        private void BobbinRunningLength_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                this.Focus(FocusState.Programmatic);
            }
            MaskNumericInput((TextBox)sender, false);
        }

        //private void runnigLength_GotFocus(object sender, RoutedEventArgs e)
        //{

        //    StackPanel panel = sender as StackPanel;
        //    if (panel != null && panel.Tag != null)
        //    {
        //        try
        //        {
        //            this.HomePagePivot.SelectedIndex = Int16.Parse(panel.Tag.ToString());
        //        }
        //        catch (FormatException) { }
        //    }
        //}
        public void UnitChangeDynamicaly()
        {
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            var gr = loader.GetString("lblShortGram");
            var m = loader.GetString("lblShortMeter");
            var ft = loader.GetString("lblShortFeet");
            var yd = loader.GetString("lblShortYards");
            var kg = loader.GetString("lblShortKilogram");
            var km = loader.GetString("lblShortKilometers");
            var lbs = loader.GetString("lblShortPound");
            var mi = loader.GetString("lblShortMiles");
            var denier = loader.GetString("lblDenier");
            var dtex = loader.GetString("lblDtex");
            try
            {

            if (Converters.UnitConverter.bobbinweightLinerd == null)
            {
                Densitytxt.Text = dtex;
            }
            if (Converters.UnitConverter.runninglengthLinerd == null)
            {
                LinearDensitytxt.Text = dtex;
            }
            if (Converters.UnitConverter.bobbinweightLinerd == dtex)
            {
                Densitytxt.Text = dtex;
            }
            if (Converters.UnitConverter.bobbinweightLinerd == denier)
            {
                Densitytxt.Text = denier;
            }
            if (Converters.UnitConverter.runninglengthLinerd == dtex)
            {
                LinearDensitytxt.Text = dtex;
            }
            if (Converters.UnitConverter.runninglengthLinerd == denier)
            {
                LinearDensitytxt.Text = denier;
            }
            if (Converters.UnitConverter.runningTube == null)
            {
                Tweighttxt.Text = gr;
            }
            if (Converters.UnitConverter.runningTube == gr)
            {
                Tweighttxt.Text = gr;
            }
            if (Converters.UnitConverter.runningTube == kg)
            {
                Tweighttxt.Text = kg;
            }
            if (Converters.UnitConverter.runningTube == lbs)
            {
                Tweighttxt.Text = lbs;
            }
            if (Converters.UnitConverter.boobinTube == null)
            {
                tubewtxt.Text = gr;
            }
            if (Converters.UnitConverter.boobinTube == gr)
            {
                tubewtxt.Text = gr;
            }
            if (Converters.UnitConverter.boobinTube == kg)
            {
                tubewtxt.Text = kg;
            }
            if (Converters.UnitConverter.boobinTube == lbs)
            {
                tubewtxt.Text = lbs;
            }

            if (Converters.UnitConverter.runningbobbinweight == null)
            {
                Bobinweighttxt.Text = gr;
            }
            if (Converters.UnitConverter.runningbobbinweight == gr)
            {
                Bobinweighttxt.Text = gr;
            }
            if (Converters.UnitConverter.runningbobbinweight == kg)
            {
                Bobinweighttxt.Text = kg;
            }
            if (Converters.UnitConverter.runningbobbinweight == lbs)
            {
                Bobinweighttxt.Text = lbs;
            }

            if (Converters.UnitConverter.bobbinRLength == null)
            {
                bobinlength.Text = m;
            }
            if (Converters.UnitConverter.bobbinRLength == m)
            {
                bobinlength.Text = m;
            }
            if (Converters.UnitConverter.bobbinRLength == km)
            {
                bobinlength.Text = km;
            }
            if (Converters.UnitConverter.bobbinRLength == ft)
            {
                bobinlength.Text = ft;
            }
            if (Converters.UnitConverter.bobbinRLength == yd)
            {
                bobinlength.Text = yd;
            }
            if (Converters.UnitConverter.bobbinRLength == mi)
            {
                bobinlength.Text = mi;
            }
                  }
            catch
            {

            }
        }
     private void MenuChange()
        {
            if (Convert.ToBoolean(isMenuOpen))
            {
                slidein.GoToMenuState(ActiveState.Right);
                {

                }
                isMenuOpen = false;
            }
            else
            {
                slidein.GoToMenuState(ActiveState.Left);
                {

                }
                isMenuOpen = true;
            }
          
        }

     private void materialL_KeyDown(object sender, KeyRoutedEventArgs e)
     {
         TextBox textboxControl = (TextBox)sender;
         if(textboxControl.Text.Contains(".") && e.Key.GetHashCode()==190)
         {
             e.Handled = true;
         }
      
     }
        private void MaskNumericInput( TextBox textboxControl,bool allowDecimal)
     {
         string[] invaildCharacters = { "#", "@", ",", "*", "(",")", "+", "-", "%", " ","'", "=","!","$","^","&", 
                                        "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
                                        "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z" 
                                      };


            for (int index=0;index<invaildCharacters.Length;index++)
            {
                textboxControl.Text = textboxControl.Text.Replace(invaildCharacters[index], "");
            }
     }

        private void tubeBoxL_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            TextBox textboxControl = (TextBox)sender;
            if (textboxControl.Text.Contains(".") && e.Key.GetHashCode() == 190)
            {
                e.Handled = true;
            }
        }

        private void TotalBobbinWeight_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            TextBox textboxControl = (TextBox)sender;
            if (textboxControl.Text.Contains(".") && e.Key.GetHashCode() == 190)
            {
                e.Handled = true;
            }
        }

        private void MaterialBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            TextBox textboxControl = (TextBox)sender;
            if (textboxControl.Text.Contains(".") && e.Key.GetHashCode() == 190)
            {
                e.Handled = true;
            }
        }

        private void TubeBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            TextBox textboxControl = (TextBox)sender;
            if (textboxControl.Text.Contains(".") && e.Key.GetHashCode() == 190)
            {
                e.Handled = true;
            }
        }

        private void BobbinRunningLength_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            TextBox textboxControl = (TextBox)sender;
            if (textboxControl.Text.Contains(".") && e.Key.GetHashCode() == 190)
            {
                e.Handled = true;
            }
        }
        //private void SetInputScope(TextBox textboxControl)
        //{
        //    InputScopeNameValue digitsInputNameValue = InputScopeNameValue.TelephoneNumber;
        //    textboxControl.InputScope = new InputScope()
        //    {
        //        Names = { new InputScopeName() { NameValue = digitsInputNameValue } }
        //    };
        //}
        //private void SetControls()
        //{
        //    SetInputScope(materialL);
        //}
    }
}


