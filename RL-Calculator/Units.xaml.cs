﻿using RL_Calculator.Converters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Http;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace RL_Calculator
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Units : Page
    {
        bool SIChecked = true;
        public static bool? isMenuOpen;
        public static Image LeftmenuTapped, Leftmenu1Tapped;
        public static TextBlock weight1, lenth1;
       
        bool IsChecked = true;
        public Units()
        {
           try 
        	{	        
		 this.InitializeComponent();
         var cult = CultureInfo.CurrentCulture.ToString();
         if (cult.Contains("zh"))
         {
             unitval.Margin = new Thickness(10, 0, 0, 0);
             //  weight.HorizontalAlignment = HorizontalAlignment.Center;
             //  lenght.HorizontalAlignment = HorizontalAlignment.Center;
             weight.Margin = new Thickness(0, 0, 10, 0);
             lenght.Margin = new Thickness(0, 0, 10, 0);
         }
        
         //ResetPageCache();
         LanguageTickImage();
        // this.NavigationCacheMode = NavigationCacheMode.Disabled;
            ScaleFactor();
            isMenuOpen = false;
            LeftmenuTapped = (Image)FindName("LeftButtonTapped");
                // Leftmenu1Tapped = (Image)FindName("LeftButton1Tapped");

                //<---------------long code unit------------>

                var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
                string gram = loader.GetString("lblUnitGramWithCode");
                var meter = loader.GetString("lblUnitMeterWithCode");
                string feet = loader.GetString("lblUnitFeetWithCode");
                string yards = loader.GetString("lblUnitYardsWithCode");
                string kilograms = loader.GetString("lblUnitKilogramWithCode");
                string kilometers = loader.GetString("lblUnitKilometersWithCode");
                string pounds = loader.GetString("lblUnitPoundWithCode");
                string miles = loader.GetString("lblUnitMilesWithCode");

                //<----EndOfStreamException------>
                var gr = loader.GetString("lblShortGram");
                var m = loader.GetString("lblShortMeter");
                var ft = loader.GetString("lblShortFeet");
                var yd = loader.GetString("lblShortYards");
                var kg = loader.GetString("lblShortKilogram");
                var km = loader.GetString("lblShortKilometers");
                var lbs = loader.GetString("lblShortPound");
                var mi = loader.GetString("lblShortMiles");
                var denier = loader.GetString("lblDenier");
                var dtex = loader.GetString("lblDtex");


                weight.Text = MainPage.runningTube.Text;
            lenght.Text = MainPage.bobbinRLength.Text;
            unitval.Text = MainPage.bobbinweightLinerd.Text;
            if (weight.Text == gr)
                weight.Text = gram;

            if (weight.Text == kg)
                weight.Text = kilograms;

            if (weight.Text == lbs)
                weight.Text = pounds;

            if (lenght.Text == m)
                lenght.Text = meter;
            if (lenght.Text == km)
                lenght.Text = kilometers;
            if (lenght.Text == ft)
                lenght.Text = feet;
            if (lenght.Text == mi)
                lenght.Text = miles;
            if (lenght.Text == yd)
                lenght.Text = yards;

               //chksi.IsChecked = true;
            if (RL_Calculator.Converters.UnitSelected.US == true)
            {
                chksi.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                //unitval.Text = MainPage.bobbinweightLinerd.Text;
                //weight.Text = MainPage.runningTube.Text;
                //lenght.Text = MainPage.bobbinRLength.Text;
               // var cultt = CultureInfo.CurrentCulture.ToString();
                if (cult.Contains("zh"))
                {
                    unitval.Margin = new Thickness(10, 0, 0, 0);
                    //  weight.HorizontalAlignment = HorizontalAlignment.Center;
                    //  lenght.HorizontalAlignment = HorizontalAlignment.Center;
                    weight.Margin = new Thickness(0, 0, 15, 0);
                    lenght.Margin = new Thickness(0, 0, 0, 0);
                }
                //unitval.Text = "denier";
                //weight.Text = "pounds (lbs)";
                //lenght.Text = "feet (ft)";

            


                OldValue.bobbinweightLinerd = MainPage.bobbinweightLinerd.Text;
                OldValue.boobinTube = MainPage.boobinTube.Text;
                OldValue.runningTube = MainPage.runningTube.Text;
                OldValue.runninglengthLinerd = MainPage.runninglengthLinerd.Text;
                OldValue.runningbobbinweight = MainPage.runningbobbinweight.Text;
                OldValue.totalbobin = MainPage.Totalbobin.Text;
                OldValue.totalrunnig = MainPage.totalrunning.Text;
                OldValue.bobbinRLength = MainPage.bobbinRLength.Text;
                UnitConverter.bobbinRLength = ft;
                UnitConverter.bobbinweightLinerd = denier;
                UnitConverter.boobinTube = lbs;



                UnitConverter.runningTube = lbs;
                UnitConverter.runninglengthLinerd = denier;
                UnitConverter.runningbobbinweight = lbs;
                chkus.Visibility = Windows.UI.Xaml.Visibility.Visible;
                IsChecked = false;
            }
            else
            {

                chksi.Visibility = Windows.UI.Xaml.Visibility.Visible;

               // var cult = CultureInfo.CurrentCulture.ToString();
                if (cult.Contains("zh"))
                {
                    unitval.Margin = new Thickness(10, 0, 0, 0);
                    //  weight.HorizontalAlignment = HorizontalAlignment.Center;
                    //  lenght.HorizontalAlignment = HorizontalAlignment.Center;
                    weight.Margin = new Thickness(0, 0, 10, 0);
                    lenght.Margin = new Thickness(0, 0, 10, 0);
                }
                OldValue.bobbinweightLinerd = MainPage.bobbinweightLinerd.Text;
                OldValue.boobinTube = MainPage.boobinTube.Text;
                OldValue.runningTube = MainPage.runningTube.Text;
                OldValue.runninglengthLinerd = MainPage.runninglengthLinerd.Text;
                OldValue.runningbobbinweight = MainPage.runningbobbinweight.Text;
                OldValue.totalbobin = MainPage.Totalbobin.Text;
                OldValue.totalrunnig = MainPage.totalrunning.Text;
                OldValue.bobbinRLength = MainPage.bobbinRLength.Text;
                UnitConverter.bobbinRLength = m;
                UnitConverter.bobbinweightLinerd = dtex;


                UnitConverter.boobinTube = gr;

                UnitConverter.runningTube = gr;
                UnitConverter.runninglengthLinerd = dtex;
                UnitConverter.runningbobbinweight = gr;

                //unitval.Text = MainPage.bobbinweightLinerd.Text;
                //weight.Text = MainPage.runningTube.Text;
                //lenght.Text = MainPage.bobbinRLength.Text;

                //unitval.Text = "dtex";
                //weight.Text = "gram (gr)";
                //lenght.Text = "meter (m)";
            }
            weight1 = (TextBlock)FindName("weight");
            lenth1 = (TextBlock)FindName("lenght");
	}
	catch (Exception ex)
	{
		  var dialog = new MessageDialog(ex.Message);
                dialog.ShowAsync();
	}
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
         //   this.NavigationCacheMode = NavigationCacheMode.Disabled;
        }
        private void LeftButtonTapped_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (Convert.ToBoolean(isMenuOpen))
            {
                slidein.GoToMenuState(ActiveState.Right);
                {

                }
                isMenuOpen = false;
            }
            else
            {
                slidein.GoToMenuState(ActiveState.Left);
                {

                }
                isMenuOpen = true;
            }
                   ////LeftButtonTapped.Visibility = Visibility.Collapsed;
                   //// LeftButton1Tapped.Visibility = Visibility.Visible;

                   // slidein.GoToMenuState(ActiveState.Left);
                   // {

                   // }

        }
        private void HomeButtonClick(object sender, TappedRoutedEventArgs e)
        {
            //<---------------long code unit------------>

            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            string gram = loader.GetString("lblUnitGramWithCode");
            var meter = loader.GetString("lblUnitMeterWithCode");
            string feet = loader.GetString("lblUnitFeetWithCode");
            string yards = loader.GetString("lblUnitYardsWithCode");
            string kilograms = loader.GetString("lblUnitKilogramWithCode");
            string kilometers = loader.GetString("lblUnitKilometersWithCode");
            string pounds = loader.GetString("lblUnitPoundWithCode");
            string miles = loader.GetString("lblUnitMilesWithCode");

            //<----EndOfStreamException------>
            var gr = loader.GetString("lblShortGram");
            var m = loader.GetString("lblShortMeter");
            var ft = loader.GetString("lblShortFeet");
            var yd = loader.GetString("lblShortYards");
            var kg = loader.GetString("lblShortKilogram");
            var km = loader.GetString("lblShortKilometers");
            var lbs = loader.GetString("lblShortPound");
            var mi = loader.GetString("lblShortMiles");
            var denier = loader.GetString("lblDenier");
            var dtex = loader.GetString("lblDtex");
            //LeftButtonTapped.Visibility = Visibility.Visible;
            //LeftButton1Tapped.Visibility = Visibility.Collapsed;

            // Metarial

            try
            {
                if (Converters.UnitConverter.bobbinweightLinerd == denier && OldValue.bobbinweightLinerd == dtex && MainPage.materialBox.Text!="")
                {

                    MainPage.materialBox.Text = Math.Round((Convert.ToDouble(MainPage.materialBox.Text) * .9),0).ToString();
                }
                if (Converters.UnitConverter.bobbinweightLinerd == dtex && OldValue.bobbinweightLinerd == denier && MainPage.materialBox.Text != "")
                {

                    MainPage.materialBox.Text = Math.Round((Convert.ToDouble(MainPage.materialBox.Text) / .9),0).ToString();
                }
                if (Converters.UnitConverter.runninglengthLinerd == denier && OldValue.runninglengthLinerd == dtex && MainPage.materialBoxL.Text!="")
                {

                    MainPage.materialBoxL.Text = Math.Round((Convert.ToDouble(MainPage.materialBoxL.Text) * .9), 0).ToString();
                }
                if (Converters.UnitConverter.runninglengthLinerd == dtex && OldValue.runninglengthLinerd == denier && MainPage.materialBoxL.Text != "")
                {

                    MainPage.materialBoxL.Text = Math.Round((Convert.ToDouble(MainPage.materialBoxL.Text) / .9),0).ToString();
                }

            }
            catch
            { }

            //// Tube

            //kg gr for bobbintube
            try
            {
                if (Converters.UnitConverter.boobinTube == kg && OldValue.boobinTube == gr && MainPage.tubeBox.Text!="")
                {

                    MainPage.tubeBox.Text = Math.Round((Convert.ToDouble(MainPage.tubeBox.Text) / 1000),3).ToString();

                    try
                    {


                        MainPage.Totalbobin.Text = Math.Round((Convert.ToDouble(MainPage.Totalbobin.Text.Replace(gr, "")) / 1000), 3).ToString() + " " + Converters.UnitConverter.boobinTube;
                    }
                    catch
                    { 
                    }
                }
                //  lbs gr
                if (Converters.UnitConverter.boobinTube == lbs && OldValue.boobinTube == gr && MainPage.tubeBox.Text!="")
                {

                    MainPage.tubeBox.Text = Math.Round((((Convert.ToDouble(MainPage.tubeBox.Text) / 453.59237) * 100000) / 100000),2).ToString();
                    try
                    {
                        MainPage.Totalbobin.Text = Math.Round((((Convert.ToDouble(MainPage.Totalbobin.Text.Replace(gr, "")) / 453.59237) * 100000) / 100000),2).ToString() + " " + Converters.UnitConverter.boobinTube;

                    }

                    catch
                    { 
                    
                    }
                
                }

                //gr kg
                if (Converters.UnitConverter.boobinTube == gr && OldValue.boobinTube == kg && MainPage.tubeBox.Text!="")
                {

                    MainPage.tubeBox.Text = Math.Round((Convert.ToDouble(MainPage.tubeBox.Text) * 1000),0).ToString();

                    try
                    {
                        MainPage.Totalbobin.Text = Math.Round((((Convert.ToDouble(MainPage.Totalbobin.Text.Replace(kg, ""))* 1000))), 0).ToString() + " " + Converters.UnitConverter.boobinTube;

                    }

                    catch
                    {

                    }
                }
                // gr lbs
                if (Converters.UnitConverter.boobinTube == gr && OldValue.boobinTube == lbs && MainPage.tubeBox.Text!="")
                {

                    MainPage.tubeBox.Text = Math.Round((((Convert.ToDouble(MainPage.tubeBox.Text) * 453.592))),0).ToString();

                    try
                    {
                        MainPage.Totalbobin.Text = Math.Round((((Convert.ToDouble(MainPage.Totalbobin.Text.Replace(lbs, "")) * 453.592))), 0).ToString() + " " + Converters.UnitConverter.boobinTube;

                    }

                    catch
                    {

                    }
                }

                // lbs kg
                if (Converters.UnitConverter.boobinTube == lbs && OldValue.boobinTube == kg && MainPage.tubeBox.Text != "")
                {

                    MainPage.tubeBox.Text = Math.Round((((Convert.ToDouble(MainPage.tubeBox.Text) * 0.45359237) * 100) / 100),2).ToString();
                    try
                    {
                        MainPage.Totalbobin.Text = Math.Round((((Convert.ToDouble(MainPage.Totalbobin.Text.Replace(kg, "")) * 0.45359237) * 100) / 100), 0).ToString() + " " + Converters.UnitConverter.boobinTube;

                    }

                    catch
                    {

                    }
                }
                // kg lbs
                if (Converters.UnitConverter.boobinTube == kg && OldValue.boobinTube == lbs && MainPage.tubeBox.Text != "")
                {

                    MainPage.tubeBox.Text = Math.Round((((Convert.ToDouble(MainPage.tubeBox.Text) * 453.592) / 1000)),3).ToString();
                    try
                    {
                        MainPage.Totalbobin.Text = Math.Round((((Convert.ToDouble(MainPage.Totalbobin.Text.Replace(kg, "")) * 453.592))), 3).ToString() + " " + Converters.UnitConverter.boobinTube;

                    }

                    catch
                    {

                    }

                }






                //kg gr for running tube
                if (Converters.UnitConverter.runningTube == kg && OldValue.runningTube == gr && MainPage.tubeL.Text != "")
                {

                    MainPage.tubeL.Text = Math.Round((Convert.ToDouble(MainPage.tubeL.Text) / 1000),3).ToString();
                }
                //  lbs gr
                if (Converters.UnitConverter.runningTube == lbs && OldValue.runningTube == gr && MainPage.tubeL.Text != "")
                {

                    MainPage.tubeL.Text = Math.Round((((Convert.ToDouble(MainPage.tubeL.Text) / 453.59237) * 100000) / 100000),2).ToString();
                }

                //gr kg
                if (Converters.UnitConverter.runningTube == gr && OldValue.runningTube == kg && MainPage.tubeL.Text != "")
                {

                    MainPage.tubeL.Text = Math.Round((Convert.ToDouble(MainPage.tubeL.Text) * 1000),0).ToString();
                }
                // gr lbs
                if (Converters.UnitConverter.runningTube == gr && OldValue.runningTube == lbs && MainPage.tubeL.Text != "")
                {

                    MainPage.tubeL.Text = Math.Round(((Convert.ToDouble(MainPage.tubeL.Text) * 453.592)),0).ToString();
                }

                // lbs kg
                if (Converters.UnitConverter.runningTube == lbs && OldValue.runningTube == kg && MainPage.tubeL.Text != "")
                {

                    MainPage.tubeL.Text = Math.Round((((Convert.ToDouble(MainPage.tubeL.Text) * 0.45359237) * 100) / 100),2).ToString();
                }
                // kg lbs
                if (Converters.UnitConverter.runningTube == kg && OldValue.runningTube == lbs && MainPage.tubeL.Text != "")
                {

                    MainPage.tubeL.Text = Math.Round((((Convert.ToDouble(MainPage.tubeL.Text) * 453.592) / 1000)),3).ToString();
                }

            }
            catch
            { 
            
            }

            //kg gr for runningbobbinweight

            try { 
            if (Converters.UnitConverter.runningbobbinweight == kg && OldValue.runningbobbinweight == gr && MainPage.TBW.Text!="")
            {

                MainPage.TBW.Text = Math.Round((Convert.ToDouble(MainPage.TBW.Text) / 1000),3).ToString();
            }
            //  lbs gr
            if (Converters.UnitConverter.runningbobbinweight == lbs && OldValue.runningbobbinweight == gr && MainPage.TBW.Text != "")
            {

                MainPage.TBW.Text = Math.Round((((Convert.ToDouble(MainPage.TBW.Text) / 453.59237) * 100000) / 100000),2).ToString();
            }

            //gr kg
            if (Converters.UnitConverter.runningbobbinweight == gr && OldValue.runningbobbinweight == kg && MainPage.TBW.Text != "")
            {

                MainPage.TBW.Text = Math.Round((Convert.ToDouble(MainPage.TBW.Text) * 1000),0).ToString();
            }
            // gr lbs
            if (Converters.UnitConverter.runningbobbinweight == gr && OldValue.runningbobbinweight == lbs && MainPage.TBW.Text != "")
            {

                MainPage.TBW.Text = Math.Round((((Convert.ToDouble(MainPage.TBW.Text) * 453.592))),0).ToString();
            }

            // lbs kg
            if (Converters.UnitConverter.runningbobbinweight == lbs && OldValue.runningbobbinweight == kg && MainPage.TBW.Text != "")
            {

                MainPage.TBW.Text = Math.Round((((Convert.ToDouble(MainPage.TBW.Text) * 0.45359237) * 100) / 100),2).ToString();
            }
            // kg lbs
            if (Converters.UnitConverter.runningbobbinweight == kg && OldValue.runningbobbinweight == lbs && MainPage.TBW.Text != "")
            {

                MainPage.TBW.Text = Math.Round((((Convert.ToDouble(MainPage.TBW.Text) * 453.592) / 1000)),3).ToString();
            }
            }
            catch { }



            //  km m for bobbinRLength
            try {

                if (Converters.UnitConverter.bobbinRLength == km && OldValue.bobbinRLength == m && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text))) / 1000),3).ToString();

                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(m, "")) / 1000))), 3).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
                
            }
            //ft m
                if (Converters.UnitConverter.bobbinRLength == ft && OldValue.bobbinRLength == m && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text)) * 3.2808399)),0).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(m, "")) * 3.2808399))), 0).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }

            }
            //yd m
                if (Converters.UnitConverter.bobbinRLength == yd && OldValue.bobbinRLength == m && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text)) * 1.09361)),0).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(m, ""))  *1.09361))), 0).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            //mi m
                if (Converters.UnitConverter.bobbinRLength == mi && OldValue.bobbinRLength == m && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text) / 1609.344) * 1000) / 1000),2).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(m, "")) / 1609.344) * 1000) / 1000), 2).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            //m km
                if (Converters.UnitConverter.bobbinRLength == m && OldValue.bobbinRLength == km && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text))) * 1000),0).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(km, "")) * 1000)) ), 0).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            //ft km
                if (Converters.UnitConverter.bobbinRLength == ft && OldValue.bobbinRLength == km && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text))) * 3280.8399),0).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(km, "")) * 3280.8399))), 0).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            //yd km
                if (Converters.UnitConverter.bobbinRLength == yd && OldValue.bobbinRLength == km && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text))) * 1093.6133),0).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(km, "")) * 1093.6133))), 0).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            //mi km
                if (Converters.UnitConverter.bobbinRLength == mi && OldValue.bobbinRLength == km && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text) / 1.609344) * 100) / 100),2).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(km, "")) / 1.609344) * 100) / 100), 2).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            //m ft
                if (Converters.UnitConverter.bobbinRLength == m && OldValue.bobbinRLength == ft && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text))) / 3.2808399),0).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(ft, "")) / 3.2808399))), 0).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            //km ft
                if (Converters.UnitConverter.bobbinRLength == km && OldValue.bobbinRLength == ft && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text) / 3280.8399) * 1000) / 1000),3).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(ft, "")) / 3280.8399) * 1000) / 1000), 3).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }

            }
            //yd ft
                if (Converters.UnitConverter.bobbinRLength == yd && OldValue.bobbinRLength == ft && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text))) / 3),0).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(ft, "")) / 3) ) ), 0).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            //mi ft
                if (Converters.UnitConverter.bobbinRLength == mi && OldValue.bobbinRLength == ft && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text) / 5280) * 1000) / 1000),2).ToString();

                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(ft, "")) / 5280) * 1000) / 1000), 2).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            //m yd
                if (Converters.UnitConverter.bobbinRLength == m && OldValue.bobbinRLength == yd && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text))) / 1.09361),0).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(yd, "")) / 1.09361) ) ), 0).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            //km yd
                if (Converters.UnitConverter.bobbinRLength == km && OldValue.bobbinRLength == yd && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text) / 1093.6133) * 1000) / 1000),3).ToString();

                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(yd, "")) / 1093.6133) * 1000) / 1000), 3).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            //ft yd
                if (Converters.UnitConverter.bobbinRLength == ft && OldValue.bobbinRLength == yd && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text))) * 3),0).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(yd, "")) *3) )), 0).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            //mi yd
                if (Converters.UnitConverter.bobbinRLength == mi && OldValue.bobbinRLength == yd && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text) / 1760) * 1000) / 1000),2).ToString();
                try
                                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(yd, "")) / 1760) * 1000) / 1000),2).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }

            }
            //m mi
                if (Converters.UnitConverter.bobbinRLength == m && OldValue.bobbinRLength == mi && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text))) * 1609.344),0).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(mi, ""))  * 1609.344))),0).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            //km mi
                if (Converters.UnitConverter.bobbinRLength == km && OldValue.bobbinRLength == mi && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text) * 1.609344) * 1000) / 1000),3).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(mi, "")) * 1.609344) * 1000) / 1000), 3).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            //ft mi
                if (Converters.UnitConverter.bobbinRLength == ft && OldValue.bobbinRLength == mi && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text)) * 5280)),0).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(mi, ""))  * 5280))), 0).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            //yd mi
                if (Converters.UnitConverter.bobbinRLength == yd && OldValue.bobbinRLength == mi && MainPage.RRL.Text != "")
            {

                MainPage.RRL.Text = Math.Round((((Convert.ToDouble(MainPage.RRL.Text)) * 1760)),0).ToString();
                try
                {
                    MainPage.totalrunning.Text = Math.Round((((Convert.ToDouble(MainPage.totalrunning.Text.Replace(mi, "")) * 1760))), 0).ToString() + " " + Converters.UnitConverter.bobbinRLength;

                }

                catch
                {

                }
            }
            }







            catch { }


      


            MainPage.bobbinweightLinerd.Text = Converters.UnitConverter.bobbinweightLinerd;
            MainPage.runninglengthLinerd.Text = Converters.UnitConverter.runninglengthLinerd;
            MainPage.bobbinRLength.Text = Converters.UnitConverter.bobbinRLength;
            MainPage.boobinTube.Text = Converters.UnitConverter.boobinTube;
            MainPage.runningTube.Text = Converters.UnitConverter.runningTube;
            MainPage.runningbobbinweight.Text = Converters.UnitConverter.runningbobbinweight;


            //LeftButtonTapped.Visibility = Visibility.Visible;
            //LeftButton1Tapped.Visibility = Visibility.Collapsed;
            //MainPage.LeftmenuTapped.Visibility = Visibility.Visible;
            //MainPage.Leftmenu1Tapped.Visibility = Visibility.Collapsed;
            this.Frame.Navigate(typeof(MainPage));
         // slidein.GoToMenuState(ActiveState.Right);

            
        }

        private void unitsButtonClick(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Units));
        }

        private void InfoButtonClick(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Info));
        }

        private void chkus_Checked(object sender, RoutedEventArgs e)
        {
            var cult = CultureInfo.CurrentCulture.ToString();
            if (cult.Contains("zh"))
            {
                unitval.Margin = new Thickness(10, 0, 0, 0);
                //  weight.HorizontalAlignment = HorizontalAlignment.Center;
                //  lenght.HorizontalAlignment = HorizontalAlignment.Center;
                weight.Margin = new Thickness(0, 0, 15, 0);
                lenght.Margin = new Thickness(0, 0, 0, 0);
            }
            //<---------------long code unit------------>

            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            string gram = loader.GetString("lblUnitGramWithCode");
            var meter = loader.GetString("lblUnitMeterWithCode");
            string feet = loader.GetString("lblUnitFeetWithCode");
            string yards = loader.GetString("lblUnitYardsWithCode");
            string kilograms = loader.GetString("lblUnitKilogramWithCode");
            string kilometers = loader.GetString("lblUnitKilometersWithCode");
            string pounds = loader.GetString("lblUnitPoundWithCode");
            string miles = loader.GetString("lblUnitMilesWithCode");

            //<----EndOfStreamException------>
            var gr = loader.GetString("lblShortGram");
            var m = loader.GetString("lblShortMeter");
            var ft = loader.GetString("lblShortFeet");
            var yd = loader.GetString("lblShortYards");
            var kg = loader.GetString("lblShortKilogram");
            var km = loader.GetString("lblShortKilometers");
            var lbs = loader.GetString("lblShortPound");
            var mi = loader.GetString("lblShortMiles");
            var denier = loader.GetString("lblDenier");
            var dtex = loader.GetString("lblDtex");


            RL_Calculator.Converters.UnitSelected.SI = false;
                    RL_Calculator.Converters.UnitSelected.US = true;
                    oldColorLength.name = "";
                    oldColorWeight.name = "";
                    IsChecked = false;
                    chksi.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    unitval.Text = denier;
                    weight.Text = pounds;
                    lenght.Text = feet;
                    OldValue.bobbinweightLinerd = MainPage.bobbinweightLinerd.Text;
                    OldValue.boobinTube = MainPage.boobinTube.Text;
                    OldValue.runningTube = MainPage.runningTube.Text;
                    OldValue.runninglengthLinerd = MainPage.runninglengthLinerd.Text;
                    OldValue.runningbobbinweight = MainPage.runningbobbinweight.Text;
                    OldValue.totalbobin = MainPage.Totalbobin.Text;
                    OldValue.totalrunnig = MainPage.totalrunning.Text;
                    OldValue.bobbinRLength = MainPage.bobbinRLength.Text;
                    UnitConverter.bobbinRLength = ft;
                    UnitConverter.bobbinweightLinerd = denier;
                    UnitConverter.boobinTube = lbs;

                    UnitConverter.runningTube = lbs;
                    UnitConverter.runninglengthLinerd = denier;
                    UnitConverter.runningbobbinweight = lbs;
                    chkus.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    isMenuOpen = false;
          
            //ImageSource img ;
            //if (chksi.IsChecked == true)
            //{
            //    chksi.IsChecked = false;
            //    chksi.Image = Image.FromFile(@"C:\Images\Dock.jpg");
            //    //   chksi.Path = "//Assets/120px-BLANK_ICON.png";
            //}
            //else
            //{
            //    chksi.IsChecked = true;
            //    //  ImgeCheckBox.Source = "/Assets/120px-BLANK_ICON.png";
            //}
            //unitval.Text = "denier";
            //weight.Text = "pounds(lbs)";
            //lenght.Text = "feet(ft)";
            //OldValue.bobbinweightLinerd = MainPage.bobbinweightLinerd.Text;
            //OldValue.boobinTube = MainPage.boobinTube.Text;
            //OldValue.runningTube = MainPage.runningTube.Text;
            //OldValue.runninglengthLinerd = MainPage.runninglengthLinerd.Text;

            //UnitConverter.bobbinRLength = "ft";
            //UnitConverter.bobbinweightLinerd = "denier";
            //UnitConverter.boobinTube = "lbs";

            //UnitConverter.runningTube = "lbs";
            //UnitConverter.runninglengthLinerd = "denier";
            //UnitConverter.runningbobbinweight = "lbs";

            //MainPage.bobbinweightLinerd.Text = "denier";
            //MainPage.boobinTube.Text = "lbs";
            //MainPage.bobbinRLength.Text = "ft";


            //MainPage.runningTube.Text = "lbs";
            //MainPage.runninglengthLinerd.Text = "denier";
            //MainPage.runningbobbinweight.Text = "lbs";
          
            //chksi.IsChecked = false;
            //chkus.IsChecked = true;
        }

        private void chksi_Checked(object sender, RoutedEventArgs e)
        {
            var cult = CultureInfo.CurrentCulture.ToString();
            if (cult.Contains("zh"))
            {
                unitval.Margin = new Thickness(10, 0, 0, 0);
                //  weight.HorizontalAlignment = HorizontalAlignment.Center;
                //  lenght.HorizontalAlignment = HorizontalAlignment.Center;
                weight.Margin = new Thickness(0, 0, 10, 0);
                lenght.Margin = new Thickness(0, 0, 10, 0);
            }
        
            //<---------------long code unit------------>

            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            string gram = loader.GetString("lblUnitGramWithCode");
            var meter = loader.GetString("lblUnitMeterWithCode");
            string feet = loader.GetString("lblUnitFeetWithCode");
            string yards = loader.GetString("lblUnitYardsWithCode");
            string kilograms = loader.GetString("lblUnitKilogramWithCode");
            string kilometers = loader.GetString("lblUnitKilometersWithCode");
            string pounds = loader.GetString("lblUnitPoundWithCode");
            string miles = loader.GetString("lblUnitMilesWithCode");

            //<----EndOfStreamException------>
            var gr = loader.GetString("lblShortGram");
            var m = loader.GetString("lblShortMeter");
            var ft = loader.GetString("lblShortFeet");
            var yd = loader.GetString("lblShortYards");
            var kg = loader.GetString("lblShortKilogram");
            var km = loader.GetString("lblShortKilometers");
            var lbs = loader.GetString("lblShortPound");
            var mi = loader.GetString("lblShortMiles");
            var denier = loader.GetString("lblDenier");
            var dtex = loader.GetString("lblDtex");


            IsChecked = true;
                    RL_Calculator.Converters.UnitSelected.SI = true;
                    RL_Calculator.Converters.UnitSelected.US = false;

                 
                    chksi.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    oldColorLength.name = "";
                    oldColorWeight.name = "";
                    OldValue.bobbinweightLinerd = MainPage.bobbinweightLinerd.Text;
                    OldValue.boobinTube = MainPage.boobinTube.Text;
                    OldValue.runningTube = MainPage.runningTube.Text;
                    OldValue.runninglengthLinerd = MainPage.runninglengthLinerd.Text;
                    OldValue.runningbobbinweight = MainPage.runningbobbinweight.Text;
                    OldValue.bobbinRLength = MainPage.bobbinRLength.Text;
                    OldValue.totalbobin = MainPage.Totalbobin.Text;
                    OldValue.totalrunnig = MainPage.totalrunning.Text;
                    UnitConverter.bobbinRLength = m;
                    UnitConverter.bobbinweightLinerd = dtex;


                    UnitConverter.boobinTube = gr;

                    UnitConverter.runningTube = gr;
                    UnitConverter.runninglengthLinerd = dtex;
                    UnitConverter.runningbobbinweight = gr;

                    //chksi.IsChecked = true;
                    //chkus.IsChecked = false;
                    unitval.Text = dtex;
                    weight.Text = gram;
                    lenght.Text = meter;
                    chkus.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

                    isMenuOpen = false;

                
            
            
            //else
            //{
            //    if (RL_Calculator.Converters.UnitSelected.US == false)
            //    {

            //        RL_Calculator.Converters.UnitSelected.SI = false;
            //        RL_Calculator.Converters.UnitSelected.US = true;


            //        IsChecked = true;
            //        chksi.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            //        unitval.Text = "denier";
            //        weight.Text = "pounds(lbs)";
            //        lenght.Text = "feet(ft)";
            //        OldValue.bobbinweightLinerd = MainPage.bobbinweightLinerd.Text;
            //        OldValue.boobinTube = MainPage.boobinTube.Text;
            //        OldValue.runningTube = MainPage.runningTube.Text;
            //        OldValue.runninglengthLinerd = MainPage.runninglengthLinerd.Text;
            //        OldValue.runningbobbinweight = MainPage.runningbobbinweight.Text;
            //        OldValue.totalbobin = MainPage.Totalbobin.Text;
            //        OldValue.totalrunnig = MainPage.totalrunning.Text;
            //        OldValue.bobbinRLength = MainPage.bobbinRLength.Text;
            //        UnitConverter.bobbinRLength = "ft";
            //        UnitConverter.bobbinweightLinerd = "denier";
            //        UnitConverter.boobinTube = "lbs";

            //        UnitConverter.runningTube = "lbs";
            //        UnitConverter.runninglengthLinerd = "denier";
            //        UnitConverter.runningbobbinweight = "lbs";
            //        chkus.Visibility = Windows.UI.Xaml.Visibility.Visible;
            //        //RL_Calculator.Converters.UnitSelected.SI = true;
            //        //RL_Calculator.Converters.UnitSelected.US = false;

            //        //IsChecked = true;
            //        //chksi.Visibility = Windows.UI.Xaml.Visibility.Visible;
            //        //OldValue.bobbinweightLinerd = MainPage.bobbinweightLinerd.Text;
            //        //OldValue.boobinTube = MainPage.boobinTube.Text;
            //        //OldValue.runningTube = MainPage.runningTube.Text;
            //        //OldValue.runninglengthLinerd = MainPage.runninglengthLinerd.Text;
            //        //OldValue.runningbobbinweight = MainPage.runningbobbinweight.Text;
            //        //OldValue.bobbinRLength = MainPage.bobbinRLength.Text;
            //        //OldValue.totalbobin = MainPage.Totalbobin.Text;
            //        //OldValue.totalrunnig = MainPage.totalrunning.Text;
            //        //UnitConverter.bobbinRLength = "m";
            //        //UnitConverter.bobbinweightLinerd = "dtex";


            //        UnitConverter.boobinTube = "gr";

            //        UnitConverter.runningTube = "gr";
            //        UnitConverter.runninglengthLinerd = "dtex";
            //        UnitConverter.runningbobbinweight = "gr";

            //        //chksi.IsChecked = true;
            //        //chkus.IsChecked = false;
            //        unitval.Text = "dtex";
            //        weight.Text = "gram(gr)";
            //        lenght.Text = "meter(m)";
            //        chkus.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            //    }
           // }

         
        //    if (chksi.IsChecked == true)
        //    {
        //        chksi.IsChecked = false;
        //    }
        //    else
        //    {
        //        chksi.IsChecked = true;
        //    }
            //MainPage.bobbinweightLinerd.Text = "dtex";
            //MainPage.bobbinRLength.Text="m";
            //MainPage.boobinTube.Text = "gr";

            //MainPage.runningTube.Text = "gr";
            //MainPage.runninglengthLinerd.Text = "dtex";
            //MainPage.runningbobbinweight.Text = "gr";
      
           
        }

        private void WeightPopUp(object sender, TappedRoutedEventArgs e)
        {
            Popup PopUpWeight;
            PopUpWeight = new Popup();
            var text = weight.Text;
         
            if (IsChecked==true)
            {
                PopUpWeight.Child =
                    new PopUpWeight("gram");
            }
            else
            {

                PopUpWeight.Child =
                    new PopUpWeight(text);
            }         
           
            PopUpWeight.IsOpen = true;

            PopUpWeight.VerticalOffset = 0;// distance of the popup from the top
            PopUpWeight.HorizontalOffset = 0;// distance of the popup from the left 
            isMenuOpen = true;

        }

        private void LengthPopUp(object sender, TappedRoutedEventArgs e)
        {
            Popup PopUpLength;
            PopUpLength = new Popup();

           
            var text = lenght.Text;

         
            if (IsChecked==true)
            {
                PopUpLength.Child =
                    new PopUpLength("meter");
            }
            else

            {

                PopUpLength.Child =
                    new PopUpLength(text);
            }
            PopUpLength.IsOpen = true;

            PopUpLength.VerticalOffset = 0;// distance of the popup from the top
            PopUpLength.HorizontalOffset = 0;// distance of the popup from the left 
            isMenuOpen = true;
        }


        //private void LeftButton1Tapped_Tapped(object sender, TappedRoutedEventArgs e)
        //{
        //    slidein.GoToMenuState(ActiveState.Right);

        //    LeftButtonTapped.Visibility = Visibility.Visible;
        //    LeftButton1Tapped.Visibility = Visibility.Collapsed;
        //}

        public void ScaleFactor()
        {
            var scaleFactor = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;
            var WidthResolution = Window.Current.Bounds.Width * scaleFactor;
            var Heightresolution = Window.Current.Bounds.Height * scaleFactor;
            if (scaleFactor == 2.2)
            {

                UnitScale.Width = 490;
                UnitScale.Height = 870;
            }
            else   if (scaleFactor == 2.4)
            {

                UnitScale.Width = 450;
                UnitScale.Height = 680;
            }
            else  if (scaleFactor == 1.8)
            {
                UnitScale.Width = 400;
                UnitScale.Height = 600;
            }
            else   if (scaleFactor == 2.0)
            {
               UnitScale.Width = 390;
                UnitScale.Height = 535;
            }
            else if (scaleFactor == 1.2 && WidthResolution != 540.0 && Heightresolution != 960.0)
            {
                UnitScale.Width = 400;
                UnitScale.Height = 640;
            }
            else if (scaleFactor == 1.2 && WidthResolution == 540.0 && Heightresolution == 960.0)
            {
                UnitScale.Width = 450;
                UnitScale.Height = 640;

            }
          
            //else if (scaleFactor == 1.6)
            //{
            //    UnitScale.Width = 480;
            //    UnitScale.Height = 740;
            //}
            else  if (scaleFactor == 3.4)
            {
                UnitScale.Width = 430;
                UnitScale.Height = 640;
            }
            else
            {
                getScreenInfo();
            }
        }
        public void getScreenInfo()
        {
            var height = Window.Current.Bounds.Height;
            var width = Window.Current.Bounds.Width;
            //var Actualheigt = height - 540;

            //var lgt = height - Actualheigt;

            UnitScale.Height = height;
            UnitScale.Width = width;

        }

        public void LanguageTickImage()
        {
            var culture = CultureInfo.CurrentCulture;
            if(culture.TextInfo.CultureName.Contains("CN"))
            {
                imgChineseTick.Visibility = Visibility.Visible;
                imgEnglishTick.Visibility = Visibility.Collapsed;
            }
            else
            {
                imgChineseTick.Visibility = Visibility.Collapsed;
                imgEnglishTick.Visibility = Visibility.Visible;
            }
        }

        string cul = "", cult = "";
        private void lblEnglish_Tapped(object sender, TappedRoutedEventArgs e)
        {
            cult = CultureInfo.CurrentCulture.ToString();
            var culture = new CultureInfo("en-US");
            cul = culture.ToString();
            Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride = culture.Name;
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;
            (Window.Current.Content as Frame).FlowDirection = Windows.UI.Xaml.FlowDirection.LeftToRight;
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            ShowDlg();
        }

        private void lblChinese_Tapped(object sender, TappedRoutedEventArgs e)
        {
            cult = CultureInfo.CurrentCulture.ToString();
            var culture = new CultureInfo("zh-CN");
            cul = culture.ToString();
            Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride = culture.Name;
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;
            (Window.Current.Content as Frame).FlowDirection = Windows.UI.Xaml.FlowDirection.LeftToRight;
            ShowDlg();
        }

        private bool _messageShowing = false;
        public async void ShowDlg()
        {
        if (!_messageShowing)
        {
        _messageShowing = true;
        var messageDialog = new MessageDialog("Teijin Aramid\n\n Do you want to proceed?");
        messageDialog.Commands.Add(new UICommand("Yes"));
        messageDialog.Commands.Add(new UICommand("No"));
        var result = await messageDialog.ShowAsync();

        if (result.Label == "Yes")
        {
        Windows.ApplicationModel.Resources.Core.ResourceContext.GetForViewIndependentUse().Reset();
        Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().Reset();
        var _Frame = Window.Current.Content as Frame;
        if (_Frame != null)
        _Frame.Navigate(_Frame.Content.GetType());
        _Frame.GoBack();
        Frame.Navigate(typeof(MainPage));
        }
    else
    {
        var PreCul = cult;
        if (cul.Contains("CN") && result.Label == "No" && PreCul == "en-US")
         {
             var culture = new CultureInfo("en-US");
             //var culture = new CultureInfo(Windows.System.UserProfile.GlobalizationPreferences.Languages[0]);
             Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride = culture.Name;
             CultureInfo.DefaultThreadCurrentCulture = culture;
             CultureInfo.DefaultThreadCurrentUICulture = culture;
             (Window.Current.Content as Frame).FlowDirection = Windows.UI.Xaml.FlowDirection.LeftToRight;
             var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
             Frame.Navigate(typeof(MainPage));
            // Frame.Navigate(typeof(Units));
             
        }
        else if (cul.Contains("US") && result.Label == "No" && PreCul == "zh-CN")
        {
            var culture = new CultureInfo("zh-CN");
            //var culture = new CultureInfo(Windows.System.UserProfile.GlobalizationPreferences.Languages[0]);
            Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride = culture.Name;
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;
            (Window.Current.Content as Frame).FlowDirection = Windows.UI.Xaml.FlowDirection.LeftToRight;
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            Frame.Navigate(typeof(MainPage));
           // Frame.Navigate(typeof(Units));
        }
        else   if (cul.Contains("US") && result.Label == "No" && PreCul == "en-US")
        {
            var culture = new CultureInfo("en-US");
            //var culture = new CultureInfo(Windows.System.UserProfile.GlobalizationPreferences.Languages[0]);
            Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride = culture.Name;
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;
            (Window.Current.Content as Frame).FlowDirection = Windows.UI.Xaml.FlowDirection.LeftToRight;
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            Frame.Navigate(typeof(MainPage));
            // Frame.Navigate(typeof(Units));
        }
        else if (cul.Contains("CN") && result.Label == "No" && PreCul == "zh-CN")
        {
            var culture = new CultureInfo("zh-CN");
            //var culture = new CultureInfo(Windows.System.UserProfile.GlobalizationPreferences.Languages[0]);
            Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride = culture.Name;
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;
            (Window.Current.Content as Frame).FlowDirection = Windows.UI.Xaml.FlowDirection.LeftToRight;
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            Frame.Navigate(typeof(MainPage));
            // Frame.Navigate(typeof(Units));
        }  
    }
    //Task<IUICommand> showMessageTask =  messageDialog.ShowAsync().AsTask();
    //await showMessageTask.ContinueWith((showAsyncResult) =>
    //    {
    //        _messageShowing = false;
    //    });
}
       }
    
          }

}
    
    



