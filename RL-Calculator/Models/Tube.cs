﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RL_Calculator
{
    class Tube
    {
        public string Dimensions { get; set; }
        public string Weight { get; set; }
        public string Selected { get; set; }
    }
}
