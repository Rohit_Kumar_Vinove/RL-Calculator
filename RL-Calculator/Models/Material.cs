﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RL_Calculator
{
    class Material
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Selected { get; set; }

        public string deniers { get; set; }

        public string LinearDensity { get; set; }
        public string ActualLinearDensity { get; set; }
    }
}
