﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace RL_Calculator.Converters
{
    public class ScaleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {          
            var resolutionScale = (int)DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel / 100;

            var baseValue = int.Parse(parameter as string);
            var scaledValue = baseValue * resolutionScale;
            if (targetType == typeof(GridLength))
                return new GridLength(scaledValue);
            return scaledValue;
        }
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
