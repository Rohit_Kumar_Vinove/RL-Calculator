﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace RL_Calculator.Converters
{
    public class IndexToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string culture)
        {
           
            int tabIndex = System.Convert.ToInt16(parameter);
            int selectedIndex = System.Convert.ToInt16(value);
            if (tabIndex == selectedIndex)
            {
                return "/Assets/tab-active-background.png";
               
               // return new SolidColorBrush(Colors.Transparent);

                
            }
            else
            {
                return "/Assets/tab-inactive-background.png";
                

              //   return new SolidColorBrush(Colors.Transparent);
              
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            throw new NotImplementedException();
        }
    }
}