﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RL_Calculator.Converters
{
    public class UnitSelected
    {
        public static bool SI { get; set; }
        public static bool US { get; set; }
        public static int Event { get; set; }
    }

}
